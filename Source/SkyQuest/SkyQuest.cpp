// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkyQuest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkyQuest, "SkyQuest" );
