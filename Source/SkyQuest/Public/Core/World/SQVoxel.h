// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SQVoxel.generated.h"

/**
 * 
 */
UCLASS()
class SKYQUEST_API USQVoxel : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable,BlueprintPure)
	static FIntVector GetChunkIndexFromWorld(FVector WorldLocation, const FIntVector& ChunkSize);
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	static FIntVector GetChunkIndex(FIntVector WorldIndex, const FIntVector& ChunkSize);
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	static FIntVector TruncWorldIndexInChunk(const FIntVector& ChunkSize, const FIntVector& WorldIndex);

	UFUNCTION(BlueprintCallable,BlueprintPure)
	static int32 GetBlockIndexInChunk(const FIntVector& ChunkSize, const FIntVector& BlockIndex);

	UFUNCTION(BlueprintCallable,BlueprintPure)
	static FIntVector GetBlockIndexInWorld(const FVector& WorldLocation);

	UFUNCTION(BlueprintCallable, BlueprintPure)
	static FIntVector GetBlockPos(int32 BlockIndex, const FIntVector& ChunkSize);
};
