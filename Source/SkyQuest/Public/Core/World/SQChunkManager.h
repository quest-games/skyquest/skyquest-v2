#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Enum/SQMeshingType.h"
#include "Struct/SQChunkToHide.h"
#include "Struct/SQChunkToShow.h"
#include "Struct/SQLoadedChunk.h"
#include "Struct/SQModifiedChunk.h"
#include "SQChunkManager.generated.h"

class USQGameInstance;
class ASQPlayerController;
class ASQWorldManager;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SKYQUEST_API USQChunkManager : public UActorComponent
{
	GENERATED_BODY()

public:
	USQChunkManager();

protected:
	virtual void BeginPlay() override;
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
public:
	TMap<FIntVector, FSQChunkToShow*> ChunksData;
	TQueue<FIntVector, EQueueMode::Mpsc> ChunksToShow;
	TQueue<FSQChunkToHide, EQueueMode::Mpsc> ChunksToHide;
	TQueue<UProceduralMeshComponent*, EQueueMode::Mpsc> FreeChunks;
	
	UPROPERTY()
	ASQWorldManager* WorldManager = nullptr;

	UPROPERTY()
	TMap<FIntVector, FSQLoadedChunk> LoadedChunks;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ESQMeshingType> MeshingType = Greedy;
	
	UPROPERTY()
	TMap<FIntVector, FSQModifiedChunk> ChunkModifications;

	int32 RenderDistance = 8;

	void SpawnChunks(const FIntVector& PlayerLocation, ASQPlayerController* PlayerController);

	void RenderChunks(const TSet<FIntVector>& ChunkIndices);
	
	UPROPERTY()
	USQGameInstance* GameInstance = nullptr;

	UPROPERTY(EditAnywhere)
	UMaterialInstance* ChunkMaterialInstance = nullptr;
	
	int32 GetBlock(const FIntVector& Index, const TArray<int16>& BlockList) const;
	
	void ShowChunk(const FSQChunkToShow* ChunkToShow);

	void HideChunk(const FSQChunkToHide& ChunkToHide);
};
