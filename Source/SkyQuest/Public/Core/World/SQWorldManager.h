#pragma once

#include "CoreMinimal.h"
#include "Core/SQGameInstance.h"
#include "GameFramework/Actor.h"
#include "Struct/SQModifiedChunk.h"
#include "Struct/SQSavedChunk.h"
#include "Struct/SQWorldBlocks.h"
#include "SQWorldManager.generated.h"

class USQWorldGenerator;
class ISQWorldGenerator;
class ASQItemStack;
class ASQPlayerController;
class USQChunk;
class USQChunkManager;
class ASQPlayerState;

UCLASS()
class SKYQUEST_API ASQWorldManager : public AActor
{
	GENERATED_BODY()
	
public:
	ASQWorldManager();

	UPROPERTY(EditAnywhere)
	TSubclassOf<ASQItemStack> ItemStackClass;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
protected:
	virtual void BeginPlay() override;
	
	mutable FCriticalSection DataGuard;
	
	UPROPERTY()
	USQGameInstance* GameInstance = nullptr;

public:
	virtual void Tick(float DeltaTime) override;
	
	TMap<FIntVector,FSQSavedChunk> SavedChunks;
	TMap<FIntVector,FSQWorldBlocks> WorldBlocks;

	TQueue<FIntVector, EQueueMode::Mpsc> ChunksToPopulate;
	bool bIsPopulating = false;

	void PopulateChunks(const TSet<FIntVector>& ChunkIndices);

	void PopulateChunk(const FIntVector& ChunkIndex);

	void ApplyModifications(const FIntVector& ChunkIndex);
	void ApplyModifications(const TSet<FIntVector>& ChunkIndices);
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	FIntVector ChunkSize = FIntVector(32);
	
	UPROPERTY()
	TArray<ASQPlayerController*> PendingConnections;
	void ConnectPlayer(ASQPlayerController* PlayerController);

	FIntVector GetSpawnableLocation(const FIntVector& ChunkIndex);

	UPROPERTY()
	USQWorldGenerator* WorldGenerator = nullptr;

	UPROPERTY(EditAnywhere)
	TSubclassOf<USQWorldGenerator> WorldGeneratorClass = nullptr;

	UPROPERTY(BlueprintReadOnly, Replicated)
	int32 Seed = 3630;

	TMap<FIntVector, TArray<FSQBlockToModify>> WorldBlockToModifies;
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	FSQBlock GetWorldBlock(const FVector& WorldLocation) const;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	USQChunkManager* ChunkManager;

	bool ModifyBlock(const FIntVector& WorldIndex, int16 NewBlock, int16& OldBlock);
	bool ModifyBlock(const FIntVector& WorldIndex, int16 NewBlock);

	void SpawnItemStack(const FVector& BlockLocation, int32 BlockId, const FIntVector& ChunkIndex);

	void SaveBlockModification(const FIntVector& ChunkIndex, int16 NewBlock, int16 Index);

	UFUNCTION(NetMulticast, Reliable)
	void Multicast_BlockUpdated(const FIntVector& WorldIndex, int16 NewBlock, int16 OldBlock);

	UFUNCTION(BlueprintCallable)
	void AddActorInChunk(const FIntVector& ChunkIndex, AActor* Actor);
	
	UFUNCTION(BlueprintCallable)
	void RemoveActorInChunk(const FIntVector& ChunkIndex, AActor* Actor);
};
