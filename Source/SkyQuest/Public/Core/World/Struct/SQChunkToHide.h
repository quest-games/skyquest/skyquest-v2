﻿#pragma once

#include "SQChunkToHide.generated.h"


class ASQPlayerController;

USTRUCT()
struct FSQChunkToHide
{
	GENERATED_BODY()
	
	UPROPERTY()
	FIntVector ChunkIndex = FIntVector(0);

	UPROPERTY()
	ASQPlayerController* PlayerController;
};