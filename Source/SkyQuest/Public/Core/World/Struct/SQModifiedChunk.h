﻿#pragma once

#include "Net/Serialization/FastArraySerializer.h"

#include "SQModifiedChunk.generated.h"

USTRUCT()
struct FSQModifiedChunkEntry : public FFastArraySerializerItem
{
	GENERATED_BODY()

	FSQModifiedChunkEntry() {}

	FSQModifiedChunkEntry(const int16 InBlockId, const int16 InPosition)
	{
		BlockId = InBlockId;
		Position = InPosition;
	}
    
	UPROPERTY()
	int16 BlockId = 0;
    
	UPROPERTY()
	int16 Position = 0;
};

USTRUCT(BlueprintType)
struct FSQModifiedChunk : public FFastArraySerializer
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<FSQModifiedChunkEntry> Modifications = {};

	bool NetDeltaSerialize(FNetDeltaSerializeInfo& DeltaParams)
	{
		return FastArrayDeltaSerialize<FSQModifiedChunkEntry, FSQModifiedChunk>(Modifications, DeltaParams, *this);
	}
};

template<>
struct TStructOpsTypeTraits<FSQModifiedChunk> : TStructOpsTypeTraitsBase2<FSQModifiedChunk>
{
	enum
	{
		WithNetDeltaSerializer = true
	};
};