﻿#pragma once

#include "ProceduralMeshComponent.h"
#include "Core/World/Struct/SQSavedActor.h"

#include "SQLoadedChunk.generated.h"

class ASQPlayerController;

USTRUCT()
struct FSQLoadedChunk
{
	GENERATED_BODY()

	FSQLoadedChunk() {}
	
	FSQLoadedChunk(UProceduralMeshComponent* Mesh, ASQPlayerController* PlayerController): Mesh(Mesh)
	{
		LoadedBy = {PlayerController};
	}
	
	UPROPERTY()
	UProceduralMeshComponent* Mesh = nullptr;

	UPROPERTY()
	TSet<ASQPlayerController*> LoadedBy = {};

	UPROPERTY()
	TSet<FSQSavedActor> Actors = {};
};
