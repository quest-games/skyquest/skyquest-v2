﻿#pragma once

#include "SQBlockToModify.h"
#include "Engine/DataTable.h"
#include "SQWorldTemplate.generated.h"

USTRUCT(BlueprintType)
struct SKYQUEST_API FSQWorldTemplate : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ID = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString Name;
	
	UPROPERTY()
	TArray<FSQBlockToModify> BlockToModifies;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FVector StructureSize = FVector(11);
};