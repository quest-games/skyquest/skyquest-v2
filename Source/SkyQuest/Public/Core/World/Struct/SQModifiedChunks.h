﻿#pragma once

#include "SQModifiedChunk.h"
#include "Net/Serialization/FastArraySerializer.h"

#include "SQModifiedChunks.generated.h"

USTRUCT()
struct FSQModifiedChunksEntry : public FFastArraySerializerItem
{
	GENERATED_BODY()

	FSQModifiedChunksEntry() {}

	FSQModifiedChunksEntry(const FIntVector& InChunkIndex, const FSQModifiedChunk& InModifiedChunk)
	{
		ChunkIndex = InChunkIndex;
		ModifiedChunk = InModifiedChunk;
	}
    
	UPROPERTY()
	FIntVector ChunkIndex = FIntVector(0);
    
	UPROPERTY()
	FSQModifiedChunk ModifiedChunk = {};
};

USTRUCT()
struct FSQModifiedChunks : public FFastArraySerializer
{
	GENERATED_BODY()

	UPROPERTY()
	TArray<FSQModifiedChunksEntry> Modifications = {};

	bool NetDeltaSerialize(FNetDeltaSerializeInfo& DeltaParams)
	{
		return FastArrayDeltaSerialize<FSQModifiedChunksEntry, FSQModifiedChunks>(Modifications, DeltaParams, *this);
	}
};

template<>
struct TStructOpsTypeTraits<FSQModifiedChunks> : TStructOpsTypeTraitsBase2<FSQModifiedChunks>
{
	enum
	{
		WithNetDeltaSerializer = true
	};
};