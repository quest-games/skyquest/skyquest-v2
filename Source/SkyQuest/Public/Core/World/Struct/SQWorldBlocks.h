﻿#pragma once

#include "SQWorldBlocks.generated.h"


USTRUCT()
struct FSQWorldBlocks
{
	GENERATED_BODY()
	
	UPROPERTY()
	TArray<int16> Blocks = {};

	UPROPERTY()
	bool bIsAir = true;
};