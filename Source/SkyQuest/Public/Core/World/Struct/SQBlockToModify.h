﻿#pragma once

#include "SQBlockToModify.generated.h"

USTRUCT()
struct SKYQUEST_API FSQBlockToModify
{
	GENERATED_BODY()

	UPROPERTY()
	FIntVector BlockIndex = FIntVector();

	UPROPERTY()
	int16 BlockType = 1;

	UPROPERTY()
	bool bIsImportant = false;

	FSQBlockToModify() { }
	
	FSQBlockToModify(const FIntVector _BlockIndex,const int16 _BlockType, const bool _bIsImportant)
	{
		BlockIndex = _BlockIndex;
		BlockType = _BlockType;
		bIsImportant = _bIsImportant;
	}
};