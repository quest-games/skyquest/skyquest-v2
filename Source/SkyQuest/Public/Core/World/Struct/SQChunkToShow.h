﻿#pragma once

#include "SQChunkToShow.generated.h"

USTRUCT()
struct FSQChunkToShow
{
	GENERATED_BODY()

	FSQChunkToShow()
	{
	}
	
	FSQChunkToShow(const FIntVector& ChunkIndex, bool bIsAir) : ChunkIndex(ChunkIndex), bIsAir(bIsAir)
	{
	}
	
	FIntVector ChunkIndex = FIntVector(0);

	bool bIsAir = true;

	TArray<FVector> Vertices = {};
	TArray<int32> Triangles = {};
	TArray<FVector> Normals = {};
	TArray<FColor> Colors = {};
	TArray<FVector2D> UVs = {};
};