﻿#pragma once

#include "SQModifiedChunk.h"
#include "SQSavedActor.h"

#include "SQSavedChunk.generated.h"


USTRUCT()
struct FSQSavedChunk
{
	GENERATED_BODY()
	
	UPROPERTY()
	FIntVector ChunkIndex = FIntVector(0);

	UPROPERTY()
	FSQModifiedChunk ModifiedChunk;

	UPROPERTY()
	TSet<FSQSavedActor> Actors = {};
};