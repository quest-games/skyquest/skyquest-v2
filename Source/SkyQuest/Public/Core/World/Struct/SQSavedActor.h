﻿#pragma once

#include "SQSavedActor.generated.h"


USTRUCT()
struct FSQSavedActor
{
	GENERATED_BODY()

	FSQSavedActor() {}

	
	FSQSavedActor(AActor* InActor)
	{
		Actor = InActor;
		ActorTransform = InActor->GetActorTransform();
	}

	UPROPERTY()
	AActor* Actor = nullptr;

	UPROPERTY()
	FTransform ActorTransform = {};
	
	bool operator ==(const FSQSavedActor& other) const
	{
		return Actor == other.Actor;
	}
};

uint32 GetTypeHash(const FSQSavedActor& SavedActor);

