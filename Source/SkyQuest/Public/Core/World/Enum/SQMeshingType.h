﻿#pragma once

#include "SQMeshingType.generated.h"

UENUM()
enum ESQMeshingType
{
	Naive,
	Greedy
};
