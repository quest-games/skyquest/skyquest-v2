﻿#pragma once

#include "Core/SQGameInstance.h"
#include "Core/World/SQVoxel.h"
#include "Core/World/Struct/SQChunkToShow.h"

class USQChunkManager;

class FSQAsyncNaiveMeshing : public FNonAbandonableTask
{
	enum ESQDirection
	{
		Forward, Right, Back, Left, Up, Down
	};

protected:
	TArray<int16> BlockList;
	FVector ChunkLocation;
	FIntVector ChunkSize;
	FIntVector ChunkIndex;
	TArray<FSQBlock> Blocks;
	FSQChunkToShow* ChunkToShow;
	USQChunkManager* ChunkManager;

public:
	FSQAsyncNaiveMeshing(const TArray<int16>& BlockList,
	                     const FVector& ChunkLocation,
	                     const FIntVector& ChunkSize,
	                     const FIntVector& ChunkIndex,
	                     const TArray<FSQBlock>& Blocks,
	                     USQChunkManager* ChunkManager,
	                     FSQChunkToShow* ChunkToShow
	) : BlockList(BlockList), ChunkLocation(ChunkLocation), ChunkSize(ChunkSize), ChunkIndex(ChunkIndex),
	    Blocks(Blocks), ChunkToShow(ChunkToShow), ChunkManager(ChunkManager)
	{
	}

	static FORCEINLINE TStatId GetStatId()
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(PrimeCalculationAsyncTask, STATGROUP_ThreadPoolAsyncTasks);
	}

	void DoWork();

	const TArray<FVector> Neighbours = {
		FVector::ForwardVector,
		FVector::RightVector,
		FVector::BackwardVector,
		FVector::LeftVector,
		FVector::UpVector,
		FVector::DownVector,
	};

	const FVector BlockVertexData[8] = {
		FVector(100, 100, 100),
		FVector(100, 0, 100),
		FVector(100, 0, 0),
		FVector(100, 100, 0),
		FVector(0, 0, 100),
		FVector(0, 100, 100),
		FVector(0, 100, 0),
		FVector(0, 0, 0)
	};

	int VertexCount = 0;

	const int BlockTriangleData[24] = {
		0, 1, 2, 3, // Forward
		5, 0, 3, 6, // Right
		4, 5, 6, 7, // Back
		1, 4, 7, 2, // Left
		5, 4, 1, 0, // Up
		3, 2, 7, 6 // Down
	};

	void CreateFace(const FIntVector& BlockIndex, ESQDirection Direction,
	                int16 BlockId);

	TArray<FVector> GetFaceVertices(ESQDirection Direction,const FVector& Position) const;
	
	FORCEINLINE int16 GetBlock(const FIntVector& BlockIndex) const
	{
		if (BlockIndex.X >= ChunkSize.X || BlockIndex.Y >= ChunkSize.Y ||
			BlockIndex.Z >= ChunkSize.Z || BlockIndex.X < 0 || BlockIndex.Y < 0 || BlockIndex.Z < 0)
			return 0;
		return BlockList[USQVoxel::GetBlockIndexInChunk(ChunkSize, BlockIndex)];
	}
	
	FORCEINLINE bool IsAir(const FIntVector& BlockIndex) const
	{
		return GetBlock(BlockIndex) == 0;
	}
};
