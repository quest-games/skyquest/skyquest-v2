﻿#pragma once

#include "Core/SQGameInstance.h"
#include "Core/World/Struct/SQChunkToShow.h"
#include "Core/World/Struct/SQMask.h"

class USQChunkManager;

class FSQAsyncGreedyMeshing : public FNonAbandonableTask
{
protected:
	TArray<int16> BlockList;
	FVector ChunkLocation;
	FIntVector ChunkSize;
	FIntVector ChunkIndex;
	TArray<FSQBlock> Blocks;
	FSQChunkToShow* ChunkToShow;
	USQChunkManager* ChunkManager;
	int32 VertexCount = 0;

public:
	FSQAsyncGreedyMeshing(const TArray<int16>& BlockList,
	                      const FVector& ChunkLocation,
	                      const FIntVector& ChunkSize,
	                      const FIntVector& ChunkIndex,
	                      const TArray<FSQBlock>& Blocks,
	                      USQChunkManager* ChunkManager,
	                      FSQChunkToShow* ChunkToShow
	) : BlockList(BlockList), ChunkLocation(ChunkLocation), ChunkSize(ChunkSize), ChunkIndex(ChunkIndex),
	    Blocks(Blocks), ChunkToShow(ChunkToShow), ChunkManager(ChunkManager)
	{
	}

	static FORCEINLINE TStatId GetStatId()
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(PrimeCalculationAsyncTask, STATGROUP_ThreadPoolAsyncTasks);
	}

	void DoWork();

	static bool CompareMask(const FSQMask M1, const FSQMask M2);

	void CreateQuad(const FSQMask& Mask, const FIntVector& AxisMask,
	                const int Width, const int Height,
	                const FIntVector& V1, const FIntVector& V2, const FIntVector& V3, const FIntVector& V4);

	int32 GetBlock(const FIntVector& Index) const;
};
