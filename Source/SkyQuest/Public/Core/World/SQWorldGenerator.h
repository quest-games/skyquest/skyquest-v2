﻿#pragma once

#include "CoreMinimal.h"
#include "Struct/SQBlockToModify.h"

#include "SQWorldGenerator.generated.h"

UCLASS(BlueprintType, Abstract)
class SKYQUEST_API USQWorldGenerator : public UObject
{
	GENERATED_BODY()

public:
	FIntVector ChunkSize = FIntVector(0);
	
	virtual bool PopulateChunk(const FIntVector& ChunkIndex, const FIntVector& InChunkSize, TMap<FIntVector, TArray<FSQBlockToModify>>& BlockToModifies, TArray<int16>& Blocks);

	void AddWorldTemplate(const int16 WorldTemplateId, const FIntVector& GlobalOriginIndex, TMap<FIntVector, TArray<FSQBlockToModify>>& BlockToModifies) const;
};
