﻿#pragma once

#include "CoreMinimal.h"
#include "Core/World/SQWorldGenerator.h"

#include "SQIslandGenerator.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Abstract, Blueprintable)
class SKYQUEST_API USQIslandGenerator : public USQWorldGenerator
{
	GENERATED_BODY()

	USQIslandGenerator();
	
	UPROPERTY(EditAnywhere)
	UCurveFloat* PerlinCurve;

	UPROPERTY(EditAnywhere)
	int32 Scale = 30;
	
	UPROPERTY(EditAnywhere)
	int32 Amplitude = 10;
	
	UPROPERTY(EditAnywhere)
	int32 Levels = 1;
	
	UPROPERTY(EditAnywhere)
	int32 ScaleFade = 2;
	
	UPROPERTY(EditAnywhere)
	int32 AmpFade = 20;
	
	UPROPERTY(EditAnywhere)
	int32 IslandSize = 300;
	
	UPROPERTY(EditAnywhere)
	TArray<FIntVector> TowersChunks = {};

	virtual bool PopulateChunk(const FIntVector& ChunkIndex, const FIntVector& InChunkSize, TMap<FIntVector, TArray<FSQBlockToModify>>& BlockToModifies, TArray<int16>& Blocks) override;
};
