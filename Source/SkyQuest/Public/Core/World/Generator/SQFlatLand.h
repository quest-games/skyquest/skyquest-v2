﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/World/SQWorldGenerator.h"
#include "SQFlatLand.generated.h"

/**
 * 
 */
UCLASS()
class SKYQUEST_API USQFlatLand : public USQWorldGenerator
{
	GENERATED_BODY()

	virtual bool PopulateChunk(const FIntVector& ChunkIndex, const FIntVector& InChunkSize, TMap<FIntVector, TArray<FSQBlockToModify>>& BlockToModifies, TArray<int16>& Blocks) override;
};
