﻿#pragma once

#include "CoreMinimal.h"
#include "Core/World/SQWorldGenerator.h"

#include "SQInfiniteLandGenerator.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class SKYQUEST_API USQInfiniteLandGenerator : public USQWorldGenerator
{
	GENERATED_BODY()

	UPROPERTY()
	int32 Scale = 30;
	
	UPROPERTY()
	int32 Amplitude = 10;
	
	UPROPERTY()
	int32 Levels = 1;
	
	UPROPERTY()
	int32 ScaleFade = 2;
	
	UPROPERTY()
	int32 AmpFade = 20;

	virtual bool PopulateChunk(const FIntVector& ChunkIndex, const FIntVector& InChunkSize, TMap<FIntVector, TArray<FSQBlockToModify>>& BlockToModifies, TArray<int16>& Blocks) override;
};
