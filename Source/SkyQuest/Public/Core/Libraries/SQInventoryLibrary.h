// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/Inventory/SQItem.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "SQInventoryLibrary.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class SKYQUEST_API USQInventoryLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	static bool CreateItem(int32 ItemType, UGameInstance* GameInstance, const int32 Stack, USQItem* &Item);

	UFUNCTION(BlueprintCallable)
	static bool CreateItemFromBlock(int32 BlockType, UGameInstance* GameInstance, const int32 Stack, USQItem* &Item);

};
