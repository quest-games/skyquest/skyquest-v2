// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "Core/Inventory/Structs/SQItemData.h"
#include "Core/Inventory/Structs/SQRecipe.h"
#include "Core/World/Struct/SQBlock.h"
#include "Core/World/Struct/SQWorldTemplate.h"
#include "SQGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SKYQUEST_API USQGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	USQGameInstance();

	UPROPERTY(BlueprintReadOnly)
	UDataTable* BlockTable;
	
	UPROPERTY(BlueprintReadOnly)
	UDataTable* ItemTable;
	
	UPROPERTY(BlueprintReadOnly)
	UDataTable* StructureTable;
	
	UPROPERTY(BlueprintReadOnly)
	UDataTable* RecipeTable;

	UPROPERTY(BlueprintReadOnly)
	TArray<FSQBlock> Blocks;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<FSQItemData> Items;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<FSQWorldTemplate> Structures;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<FSQRecipe> Recipes;

	UFUNCTION(BlueprintCallable,BlueprintPure)
	bool GetBlock(int32 ID,FSQBlock& Block) const;
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	bool GetItem(int32 ID,FSQItemData& Item) const;
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	bool GetStructure(int32 ID,FSQWorldTemplate& Structure) const;
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	bool GetRecipe(int32 ID, FSQRecipe& Recipe) const;

	UPROPERTY(BlueprintReadWrite,EditAnywhere, meta=(ClampMin=2, Delta=2))
	int32 RenderDistance = 4;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	FIntVector SpawnLocation = FIntVector(0,0,0);

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	int32 Seed = 3630;

	void ReadConfigFile();
};
