// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Structs/SQItemData.h"
#include "SQItem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStackUpdated);

class USQInventory;
/**
 * 
 */
UCLASS(BlueprintType)
class SKYQUEST_API USQItem : public UObject
{
	GENERATED_BODY()

	virtual UWorld* GetWorld() const override;
	
	UFUNCTION(BlueprintPure, Category = "My Object")
	AActor* GetOwningActor() const;
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual bool IsSupportedForNetworking() const override;

	virtual int32 GetFunctionCallspace(UFunction* Function, FFrame* Stack) override;

	virtual bool CallRemoteFunction(UFunction* Function, void* Parms, FOutParmRec* OutParms, FFrame* Stack) override;

public:

	UFUNCTION(BlueprintCallable)
	static USQItem* CreateItem(int32 ItemId, int32 Stack, UObject* Outer);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FSQItemData GetItemData();
	
	UPROPERTY(BlueprintReadWrite, Replicated)
	int32 ItemId = 1;
	
	UPROPERTY(BlueprintReadWrite, ReplicatedUsing=OnRep_Stack)
	int32 Stack = 1;

	UFUNCTION()
	void OnRep_Stack();

	UPROPERTY(BlueprintAssignable)
	FOnStackUpdated OnStackUpdated;

	UFUNCTION(BlueprintCallable)
	bool IsFullStack();
};
