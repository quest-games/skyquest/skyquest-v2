// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Core/Inventory/SQItem.h"
#include "SQInventory.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class SKYQUEST_API USQInventory : public UActorComponent
{
	
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USQInventory();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite, ReplicatedUsing=OnRep_Items)
	TArray<USQItem*> Items;

	UFUNCTION()
	void OnRep_Items();

	UFUNCTION(BlueprintImplementableEvent)
	void OnInventoryUpdated();

	UFUNCTION(BlueprintImplementableEvent)
	void OnToolbarUpdated();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 SizeX = 5;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 SizeY = 5;

	UFUNCTION(BlueprintCallable)
	virtual bool AddItemAt(USQItem* ItemToAdd, int32 X, int32 Y, USQItem* &RestItem);

	UFUNCTION(BlueprintCallable)
	bool RemoveItemAt(int32 X, int32 Y, USQItem* &RemovedItem);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetItem(int32 X, int32 Y, USQItem* &Item);

	UFUNCTION(BlueprintCallable)
	void SetItem(const FIntPoint& ItemIndex, USQItem* Item);

	UFUNCTION(BlueprintCallable)
	virtual bool AddItem(USQItem* ItemToAdd, USQItem*& RestItem);

	bool GetAvailableSlot(const int32 ItemID, int32& X, int32& Y);

	bool GetFreeSlot(int32& X, int32& Y);

	UFUNCTION(BlueprintCallable)
	virtual bool TakeItemAt(int32 X, int32 Y, int32 Quantity, USQItem*& QuantityTaked);

	UFUNCTION(BlueprintCallable)
	virtual bool TakeItem(int32 ItemType, int32 Quantity, USQItem*& ItemTaked);

	UFUNCTION(BlueprintCallable)
	int32 GetQuantityItem(const int32 Item);

	UFUNCTION(BlueprintCallable)
	bool SplitStack(int32 X, int32 Y, USQItem*& StackSplited);
	
	UFUNCTION(BlueprintCallable)
	bool GetQuantityInItem(USQItem* Item, const int32 QuantityToSplit, USQItem*& OriginStack, USQItem*& NewStack);
};
