﻿#pragma once
#include "Engine/DataTable.h"

#include "SQItemData.Generated.h"

USTRUCT(BlueprintType)
struct SKYQUEST_API FSQItemData : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ID = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString Name;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName DisplayName;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 MaxStack = 64;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UTexture2D* Icon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 BlockID = 0;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TMap<FName,int32> ItemTags;
};