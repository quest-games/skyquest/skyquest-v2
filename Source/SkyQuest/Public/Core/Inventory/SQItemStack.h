﻿#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "SQItemStack.generated.h"

class USQItem;

UCLASS()
class SKYQUEST_API ASQItemStack : public AActor
{
	GENERATED_BODY()

public:
	ASQItemStack();

protected:
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void Destroyed() override;
public:
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USphereComponent* SphereComponent;

	UPROPERTY(BlueprintReadOnly)
	FIntVector ChunkIndex; 

	UPROPERTY(BlueprintReadWrite)
	USQItem* Item;

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing=OnRep_ItemId)
	int32 ItemId;
	
	UFUNCTION()
	void OnRep_ItemId();

	UFUNCTION(BlueprintImplementableEvent)
	void OnItemChange();
};
