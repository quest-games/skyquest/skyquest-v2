#pragma once

#include "CoreMinimal.h"
#include "Core/World/Struct/SQModifiedChunks.h"
#include "GameFramework/PlayerController.h"
#include "SQPlayerController.generated.h"

class ASQCharacter;
class ASQWorldManager;
class USQChunkManager;
class UInputMappingContext;

UCLASS()
class SKYQUEST_API ASQPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ASQPlayerController();
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UInputMappingContext* InputMappingContext = nullptr;

	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void Tick(float DeltaSeconds) override;

public:
	UPROPERTY(EditAnywhere)
	TSubclassOf<ASQCharacter> CharacterClass;

	TSet<FIntVector> ChunksToRender;
	TArray<FIntVector> ChunksToFetch;
	
	UPROPERTY(BlueprintReadOnly)
	ASQWorldManager* WorldManager = nullptr;

	UPROPERTY(ReplicatedUsing=OnRep_PlayerLocation, BlueprintReadOnly)
	FIntVector PlayerLocation = FIntVector(0);

	UFUNCTION()
	void OnRep_PlayerLocation();
	
	UFUNCTION(Client,Reliable)
	void Client_Connect();
	
	UFUNCTION(Server,Reliable)
	void Server_Connect();

	UFUNCTION(Client, Reliable)
	void Client_SpawnChunks();

	void Disconnect() const;
	
	void RenderChunks() const;

	void SpawnPlayer();

	UFUNCTION(Server,Reliable)
	void Server_SpawnPlayer();

	UFUNCTION(Client,Reliable)
	void Client_FetchChunks(const TArray<FIntVector>& ChunkIndices);

	UFUNCTION(Server, Reliable)
	void Server_FetchChunks(const TArray<FIntVector>& ChunkIndices);

	UFUNCTION(Client, Reliable)
	void Client_SetFetchedChunks(const FSQModifiedChunks& ModifiedChunks);
};
