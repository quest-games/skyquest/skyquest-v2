#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SQGameMode.generated.h"

UCLASS()
class SKYQUEST_API ASQGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASQGameMode();

	virtual void Logout(AController* Exiting) override;
};
