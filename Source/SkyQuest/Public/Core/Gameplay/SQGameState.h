#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "SQGameState.generated.h"

class ASQWorldManager;

UCLASS()
class SKYQUEST_API ASQGameState : public AGameStateBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	void SpawnWorldManager();

	UPROPERTY(EditAnywhere)
	TSubclassOf<ASQWorldManager> WorldManagerClass;
	
public:
	
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing=OnRep_WorldManager)
	ASQWorldManager* WorldManager;

	UFUNCTION()
	void OnRep_WorldManager() const;
};
