#pragma once

#include "CoreMinimal.h"
#include "InputAction.h"
#include "Camera/CameraComponent.h"
#include "Core/Gameplay/SQPlayerController.h"
#include "Core/Inventory/SQCrafter.h"
#include "Core/Inventory/SQInventory.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetSystemLibrary.h"
#include "SQCharacter.generated.h"

UCLASS()
class SKYQUEST_API ASQCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ASQCharacter();

protected:
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
public:
	UPROPERTY()
	ASQPlayerController* PlayerController;
	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, ReplicatedUsing=OnRep_Inventory)
	USQInventory* Inventory;

	UPROPERTY()
	ASQWorldManager* WorldManager;

	UFUNCTION()
	void OnRep_Inventory();
	
	UFUNCTION(BlueprintImplementableEvent)
	void OnInventoryReady();
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<USQInventory> InventoryClass;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	USQCrafter* Crafter;

	UPROPERTY(BlueprintReadOnly)
	UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputMappingContext* InputMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* JumpAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* MoveAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* DebugAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* LookAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* SprintAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* DestroyBlockAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* PlaceBlockAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* SelectItemAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* DropItemAction;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInputAction* InteractAction;

	void Move(const FInputActionValue& Value);
	void Look(const FInputActionValue& Value);

	UFUNCTION(Server, Unreliable)
	void Server_SetCameraOrientation(float NewOrientation);
	
	float CameraVerticalOrientation = 0.0f;
	
	void Debug(const FInputActionValue& Value);

	UFUNCTION(Server, Reliable)
	void Server_Debug();
	
	void StartSprint(const FInputActionValue& Value);
	void StopSprint(const FInputActionValue& Value);
	
	void StartJump();
	
	void SelectItem(const FInputActionValue& Value);

	void DropItem();

	UFUNCTION(Server,Reliable)
	void Server_DropItem();

	UFUNCTION(Server, Reliable)
	void Server_ChangeSelectedItem(int32 NewSelectedItem);

	UFUNCTION(Server, Reliable)
	void Server_StartSprint();
	UFUNCTION(Client, Reliable)
	void Client_StartSprint();
	
	UFUNCTION(Server, Reliable)
	void Server_StopSprint();	
	UFUNCTION(Client, Reliable)
	void Client_StopSprint();

	UFUNCTION(BlueprintCallable)
	FHitResult Raycast(float Length, TEnumAsByte<EDrawDebugTrace::Type> DrawDebug = EDrawDebugTrace::None);

	void DestroyBlock();
	void PlaceBlock();

	UFUNCTION(Server, Reliable)
	void Server_PlaceBlock();

	UFUNCTION(Server, Reliable)
	void Server_DestroyBlock();

	UPROPERTY(ReplicatedUsing=OnRep_SelectedItem, BlueprintReadOnly)
	int32 SelectedItem = 0;

	UFUNCTION()
	void OnRep_SelectedItem() const;

	UFUNCTION(BlueprintImplementableEvent)
	void Interact();
};
