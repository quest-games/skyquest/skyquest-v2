// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Inventory/SQInventory.h"

#include "Core/Libraries/SQInventoryLibrary.h"
#include "Net/UnrealNetwork.h"
#include "Engine/ActorChannel.h"
#include "Net/Core/Misc/NetConditionGroupManager.h"
#include "Net/Core/PushModel/PushModel.h"

// Sets default values for this component's properties
USQInventory::USQInventory()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	
	bReplicateUsingRegisteredSubObjectList = true;
}


// Called when the game starts
void USQInventory::BeginPlay()
{
	Super::BeginPlay();
	
	Items.SetNum(SizeX * SizeY);
}

void USQInventory::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	DOREPLIFETIME_WITH_PARAMS_FAST(USQInventory, Items, Params)
}


// Called every frame
void USQInventory::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void USQInventory::OnRep_Items()
{
	OnInventoryUpdated();
}

bool USQInventory::AddItemAt(USQItem* ItemToAdd, int32 X, int32 Y, USQItem*& RestItem)
{
	bool bResult;
	const int32 Index = X * SizeY + Y;
	const FSQItemData ItemAsset = ItemToAdd->GetItemData();
	if(Items.IsValidIndex(Index))
	{
		USQItem* ItemInSlot = Items[Index];
		if (ItemInSlot == nullptr)
		{
			Items[Index] = ItemToAdd;
			RestItem = nullptr;
			bResult = true;
		}
		else if (ItemAsset.ID == ItemInSlot->ItemId)
		{
			if (ItemInSlot->Stack + ItemToAdd->Stack <= ItemAsset.MaxStack)
			{
				ItemInSlot->Stack += ItemToAdd->Stack;
				RestItem = nullptr;
				bResult = true;
			}
			else
			{
				RestItem = ItemToAdd;
				RestItem->Stack = ItemInSlot->Stack + ItemToAdd->Stack - ItemAsset.MaxStack;
				ItemInSlot->Stack = ItemAsset.MaxStack;
				bResult = false;
			}
		}
		else
		{
			RestItem = ItemInSlot;
			Items[Index] = ItemToAdd;
			bResult = true;
		}
	}
	else
		bResult = false;

	if (bResult)
	{
		AddReplicatedSubObject(ItemToAdd);
		MARK_PROPERTY_DIRTY_FROM_NAME(USQInventory, Items, this)
	}
	return bResult;
}

bool USQInventory::RemoveItemAt(int32 X, int32 Y, USQItem*& RemovedItem)
{
	const int32 Index = X * SizeY + Y;
	if (Items.IsValidIndex(Index))
	{
		RemovedItem = Items[Index];
		if (RemovedItem)
		{
			Items[Index] = nullptr;
			RemoveReplicatedSubObject(RemovedItem);
			return true;
		}
	}

	return false;
}

bool USQInventory::GetItem(int32 X, int32 Y, USQItem*& Item)
{
	const int32 Index = X * SizeY + Y;
	if (Items.IsValidIndex(Index))
	{
		Item = Items[Index];
		return IsValid(Item);
	}

	return false;
}

void USQInventory::SetItem(const FIntPoint& ItemIndex, USQItem* Item)
{
	Items[ItemIndex.X * SizeY + ItemIndex.Y] = Item;
}

bool USQInventory::AddItem(USQItem* ItemToAdd, USQItem*& RestItem)
{
	bool bSuccess = false;
	if (!IsValid(ItemToAdd))
		return false;
	int32 RestStack = ItemToAdd->Stack;
	
	for (USQItem* Item : Items)
	{
		if (Item && RestStack > 0)
		{
			if (!Item->IsFullStack() && ItemToAdd->ItemId == Item->ItemId)
			{
				const int32 NewQuantity = RestStack + Item->Stack;
				Item->Stack = FMath::Clamp(NewQuantity,0,Item->GetItemData().MaxStack);
				RestStack = NewQuantity - Item->GetItemData().MaxStack;
			}
		}
	}
	ItemToAdd->Stack = RestStack;
	if (RestStack > 0)
	{
		for (int y = 0; y < SizeY; ++y)
		{
			for (int x = 0; x < SizeX; ++x)
			{
				const USQItem* Item = Items[x * SizeY + y];
				if (!Item && RestStack > 0)
				{
					AddItemAt(ItemToAdd,x,y,RestItem);
					RestStack = 0;
					bSuccess = true;
					break;
				}
			}
		}
	}
	else
	{
		bSuccess = true;
	}
	if (RestStack > 0)
		RestItem = ItemToAdd;
	
	return bSuccess;
}

bool USQInventory::GetAvailableSlot(const int32 ItemID, int32& X, int32& Y)
{
	if (ItemID > 0)
		return false;
	for (int x = 0; x < SizeX; ++x)
	{
		for (int y = 0; y < SizeY; ++y)
		{
			const int32 Index = x * SizeY + y;
			USQItem* ItemInSlot = Items[Index];
			if (ItemInSlot != nullptr && ItemInSlot->ItemId == ItemID && !ItemInSlot->IsFullStack())
			{
				X = x;
				Y = y;
				return true;
			}
		}
	}
	return false;
}

bool USQInventory::GetFreeSlot(int32& X, int32& Y)
{
	for (int x = 0; x < SizeX; ++x)
	{
		for (int y = 0; y < SizeY; ++y)
		{
			const int32 Index = x * SizeY + y;
			if (Items[Index] == nullptr)
			{
				X = x;
				Y = y;
				return true;
			}
		}
	}
	return false;
}

bool USQInventory::TakeItemAt(int32 X, int32 Y, int32 Quantity, USQItem*& QuantityTaked)
{
	const int32 Index = X * SizeY + Y;
	USQItem* ItemInSlot = Items[Index];
	if (ItemInSlot != nullptr)
	{
		if (ItemInSlot->Stack - Quantity == 0)
		{
			QuantityTaked = ItemInSlot;
			Items[Index] = nullptr;
		}
		else if (ItemInSlot->Stack - Quantity > 0)
		{
			QuantityTaked = USQItem::CreateItem(ItemInSlot->ItemId, Quantity, this);
			Items[Index]->Stack -= Quantity;
		}
		else
		{
			QuantityTaked = USQItem::CreateItem(ItemInSlot->ItemId, ItemInSlot->Stack, this);
			Items[Index] = nullptr;
		}
		return true;
	}

	return false;
}

bool USQInventory::TakeItem(int32 ItemType, int32 Quantity, USQItem*& ItemTaked)
{
	int32 i = 0;
	USQInventoryLibrary::CreateItem(ItemType, GetWorld()->GetGameInstance(),0,ItemTaked);
	for (USQItem* Item : Items)
	{
		if (IsValid(Item) && Item->ItemId == ItemType)
		{
			if (Item->Stack > Quantity)
			{
				ItemTaked->Stack = Quantity;
				Item->Stack -= Quantity;
				break;
			}
			
			if (Item->Stack == Quantity)
			{
				ItemTaked->Stack = Quantity;
				Items[i] = nullptr;
				break;
			}

			ItemTaked->Stack += Item->Stack;
			Items[i] = nullptr;
		}
		i++;
	}

	return  Quantity <= 0;
}

int32 USQInventory::GetQuantityItem(const int32 Item)
{
	int32 Count = 0;

	for (const USQItem* InventoryItem : Items)
	{
		if (IsValid(InventoryItem) && InventoryItem->ItemId == Item)
			Count += InventoryItem->Stack;
	}
	
	return Count;
}

bool USQInventory::SplitStack(const int32 X,const int32 Y, USQItem*& StackSplited)
{
	const int32 Index = X * SizeY + Y;
	if (USQItem* ItemInSlot = Items[Index])
	{
		if (ItemInSlot->Stack > 1)
		{
			const int32 QuantityTaked = ItemInSlot->Stack / 2;
			USQInventoryLibrary::CreateItem(ItemInSlot->ItemId,
				GetWorld()->GetGameInstance(), QuantityTaked, StackSplited);
			Items[Index]->Stack -= QuantityTaked;
			AddReplicatedSubObject(StackSplited);
		}
		else
		{
			StackSplited = ItemInSlot;
			Items[Index] = nullptr;
		}
		return true;
	}

	return false;
}

bool USQInventory::GetQuantityInItem(USQItem* Item, const int32 QuantityToSplit, USQItem*& OriginStack, USQItem*& NewStack)
{
	if (Item)
	{
		OriginStack = Item;
		if (Item->Stack > QuantityToSplit)
		{
			USQInventoryLibrary::CreateItem(Item->ItemId,
				GetWorld()->GetGameInstance(), QuantityToSplit, NewStack);
			OriginStack->Stack -= QuantityToSplit;
		}
		else
		{
			NewStack = OriginStack;
			OriginStack = nullptr;
		}
		return true;
	}

	return false;
}
