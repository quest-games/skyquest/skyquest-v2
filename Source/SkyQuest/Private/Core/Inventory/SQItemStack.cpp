﻿#include "Core/Inventory/SQItemStack.h"

#include "Core/Gameplay/SQGameState.h"
#include "Core/Inventory/SQItem.h"
#include "Core/World/SQVoxel.h"
#include "Core/World/SQWorldManager.h"
#include "Net/UnrealNetwork.h"

ASQItemStack::ASQItemStack()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMeshComponent->SetupAttachment(RootComponent);
	
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereComponent->SetupAttachment(StaticMeshComponent);
}

void ASQItemStack::BeginPlay()
{
	Super::BeginPlay();

	if (GetNetMode() == NM_DedicatedServer)
	{
		ItemId = Item->ItemId;
	}

	ASQWorldManager* WorldManager = Cast<ASQGameState>(GetWorld()->GetGameState())->WorldManager;
	ChunkIndex = USQVoxel::GetChunkIndexFromWorld(GetActorLocation(),WorldManager->ChunkSize);
	WorldManager->AddActorInChunk(ChunkIndex,this);
}

void ASQItemStack::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	DOREPLIFETIME_WITH_PARAMS_FAST(ASQItemStack, ItemId, Params)
}

void ASQItemStack::Destroyed()
{
	ASQWorldManager* WorldManager = Cast<ASQGameState>(GetWorld()->GetGameState())->WorldManager;

	WorldManager->RemoveActorInChunk(ChunkIndex,this);
	
	Super::Destroyed();
}

void ASQItemStack::OnRep_ItemId()
{
	OnItemChange();
}

void ASQItemStack::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

