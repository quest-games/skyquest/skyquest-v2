// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Inventory/SQItem.h"

#include "Core/SQGameInstance.h"
#include "Core/Inventory/SQInventory.h"
#include "Net/UnrealNetwork.h"

UWorld* USQItem::GetWorld() const
{
	if (const UObject* MyOuter = GetOuter())
	{
		return MyOuter->GetWorld();
	}
	return nullptr;
}

AActor* USQItem::GetOwningActor() const
{
	return GetTypedOuter<AActor>();
}

void USQItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	DOREPLIFETIME_WITH_PARAMS_FAST(USQItem, ItemId, Params)
	DOREPLIFETIME_WITH_PARAMS_FAST(USQItem, Stack, Params)

	if (const UBlueprintGeneratedClass* BPClass = Cast<UBlueprintGeneratedClass>(GetClass()))
	{
		BPClass->GetLifetimeBlueprintReplicationList(OutLifetimeProps);
	}
}

bool USQItem::IsSupportedForNetworking() const
{
	return true;
}

int32 USQItem::GetFunctionCallspace(UFunction* Function, FFrame* FrameStack)
{
	check(GetOuter() != nullptr);
	return GetOuter()->GetFunctionCallspace(Function, FrameStack);
}

bool USQItem::CallRemoteFunction(UFunction* Function, void* Parms, FOutParmRec* OutParms, FFrame* FrameStack)
{
	check(!HasAnyFlags(RF_ClassDefaultObject));
	AActor* Owner = GetOwningActor();
	UNetDriver* NetDriver = Owner->GetNetDriver();
	if (NetDriver)
	{
		NetDriver->ProcessRemoteFunction(Owner, Function, Parms, OutParms, FrameStack, this);
		return true;
	}
	return false;
}

USQItem* USQItem::CreateItem(int32 ItemId, int32 Stack, UObject* Outer)
{
	USQItem* Item = NewObject<USQItem>(Outer);
	Item->ItemId = ItemId;
	Item->Stack = Stack;
	return Item;
}

FSQItemData USQItem::GetItemData()
{
	USQGameInstance* GameInstance = Cast<USQGameInstance>(GetWorld()->GetGameInstance());
	FSQItemData ItemData;
	GameInstance->GetItem(ItemId,ItemData);
	return ItemData;
}

void USQItem::OnRep_Stack()
{
	OnStackUpdated.Broadcast();
}

bool USQItem::IsFullStack()
{
	return Stack == GetItemData().MaxStack;
}
