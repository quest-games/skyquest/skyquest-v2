#include "Core/Gameplay/SQGameMode.h"

#include "Core/Gameplay/SQGameState.h"
#include "Core/Gameplay/SQPlayerController.h"
#include "Core/Gameplay/SQPlayerState.h"

ASQGameMode::ASQGameMode()
{
	GameStateClass = ASQGameState::StaticClass();
	PlayerControllerClass = ASQPlayerController::StaticClass();
	PlayerStateClass = ASQPlayerState::StaticClass();
	DefaultPawnClass = nullptr;
}

void ASQGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	ASQPlayerController* PlayerController = Cast<ASQPlayerController>(Exiting);
	PlayerController->Disconnect();
}
