#include "Core/Gameplay/SQPlayerController.h"
#include "EnhancedInputSubsystems.h"
#include "Core/Character/SQCharacter.h"
#include "Core/Gameplay/SQGameState.h"
#include "Core/World/SQChunkManager.h"
#include "Core/World/SQVoxel.h"
#include "Core/World/SQWorldManager.h"
#include "Net/UnrealNetwork.h"

ASQPlayerController::ASQPlayerController()
{
}

void ASQPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(InputMappingContext, 0);

		UE_LOG(LogTemp, Warning, TEXT("BeginPlay"));
	}
}

void ASQPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	DOREPLIFETIME_WITH_PARAMS_FAST(ASQPlayerController, PlayerLocation, Params);
}

void ASQPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (GetPawn() && GetNetMode() == NM_DedicatedServer && WorldManager)
	{
		FIntVector CurrentPlayerLocation = USQVoxel::GetChunkIndexFromWorld(GetPawn()->GetActorLocation(),WorldManager->ChunkSize);
		if (PlayerLocation != CurrentPlayerLocation)
	 	{
			WorldManager->RemoveActorInChunk(PlayerLocation, GetPawn());
			WorldManager->AddActorInChunk(CurrentPlayerLocation, GetPawn());
			
			PlayerLocation = CurrentPlayerLocation;
 			WorldManager->ChunkManager->SpawnChunks(PlayerLocation, this);
		}
	}
}

void ASQPlayerController::Client_Connect_Implementation()
{
	PlayerLocation = Cast<USQGameInstance>(GetGameInstance())->SpawnLocation;
	
	Server_Connect();
}

void ASQPlayerController::Server_Connect_Implementation()
{
	PlayerLocation = Cast<USQGameInstance>(GetGameInstance())->SpawnLocation;

	WorldManager = Cast<ASQGameState>(GetWorld()->GetGameState())->WorldManager;
	
	WorldManager->ConnectPlayer(this);
}

void ASQPlayerController::Client_SpawnChunks_Implementation()
{
	if (WorldManager)
	{
		WorldManager->ChunkManager->SpawnChunks(PlayerLocation, this);
	}
}

void ASQPlayerController::OnRep_PlayerLocation()
{
	if (WorldManager)
	{
		WorldManager->ChunkManager->SpawnChunks(PlayerLocation, this);
	}
}

void ASQPlayerController::Disconnect() const
{
	if (GetPawn())
	{
		GetPawn()->Destroy();
	}
}

void ASQPlayerController::RenderChunks() const
{
	if (ChunksToFetch.IsEmpty() && !WorldManager->bIsPopulating)
	{
		AsyncTask(ENamedThreads::AnyBackgroundThreadNormalTask,[this]
		{
			WorldManager->ApplyModifications(ChunksToRender);
			WorldManager->ChunkManager->RenderChunks(ChunksToRender);
		});
	}
}

void ASQPlayerController::SpawnPlayer()
{
	Server_SpawnPlayer();
}

void ASQPlayerController::Client_SetFetchedChunks_Implementation(const FSQModifiedChunks& ModifiedChunks)
{
	for (int i = 0; i < ChunksToFetch.Num(); i++)
	{
		FSQSavedChunk SavedChunk = FSQSavedChunk();
		SavedChunk.ChunkIndex = ChunksToFetch[i];
		SavedChunk.ModifiedChunk = ModifiedChunks.Modifications[i].ModifiedChunk;
		WorldManager->SavedChunks.Add(ChunksToFetch[i], SavedChunk);
	}
	
	ChunksToFetch.Empty();
	
	if (!ModifiedChunks.Modifications.IsEmpty())
	{
		RenderChunks();
	}
}

void ASQPlayerController::Server_SpawnPlayer_Implementation()
{
	if (GetPawn())
	{
		return;
	}
	
	const FIntVector BlockPos = WorldManager->GetSpawnableLocation(PlayerLocation);
	
	if (BlockPos == FIntVector(-1))
	{
		GEngine->AddOnScreenDebugMessage(-1,5,FColor::Red,TEXT("BAD SPAWN LOCATION"));
		return;
	}
	
	const FVector WorldPos = FVector(PlayerLocation * WorldManager->ChunkSize + BlockPos) * 100 + 50;
	const FTransform SpawnTransform = FTransform(FRotator(0),WorldPos,FVector(1));
	ASQCharacter* PlayerCharacter = GetWorld()->SpawnActorDeferred<ASQCharacter>(CharacterClass,SpawnTransform);
	PlayerCharacter->Owner = this;
	PlayerCharacter->FinishSpawning(SpawnTransform);
	Possess(PlayerCharacter);
}

void ASQPlayerController::Client_FetchChunks_Implementation(const TArray<FIntVector>& ChunkIndices)
{
	ChunksToFetch = ChunkIndices;
	Server_FetchChunks(ChunkIndices);
}

void ASQPlayerController::Server_FetchChunks_Implementation(const TArray<FIntVector>& ChunkIndices)
{
	FSQModifiedChunks ModifiedChunks = {};
	for (const FIntVector& ChunkIndex : ChunkIndices)
	{
		FSQModifiedChunk ModifiedChunk = FSQModifiedChunk();
		if (FSQSavedChunk* SavedChunk = WorldManager->SavedChunks.Find(ChunkIndex))
		{
			ModifiedChunk = SavedChunk->ModifiedChunk;
		}
		ModifiedChunks.Modifications.Add({ChunkIndex, ModifiedChunk});
	}

	Client_SetFetchedChunks(ModifiedChunks);
}


