#include "Core/Gameplay/SQGameState.h"

#include "Core/Gameplay/SQPlayerController.h"
#include "Core/Libraries/SQPerlinLibrary.h"
#include "Core/World/SQWorldManager.h"
#include "OnlineSubsystem.h"
#include "Core/World/SQVoxel.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

void ASQGameState::BeginPlay()
{
	Super::BeginPlay();

	const ENetMode NetMode = GetNetMode();
	if (NetMode == NM_DedicatedServer)
	{
		SpawnWorldManager();
	}
}

void ASQGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	DOREPLIFETIME_WITH_PARAMS_FAST(ASQGameState, WorldManager, Params)
}

void ASQGameState::SpawnWorldManager()
{
	UE_LOG(LogTemp,Warning, TEXT("Spawn World Manager"))

	WorldManager = GetWorld()->SpawnActorDeferred<ASQWorldManager>(
		WorldManagerClass,
		FTransform(),
		this,
		nullptr,
		ESpawnActorCollisionHandlingMethod::AlwaysSpawn
		);

	const USQGameInstance* GameInstance = Cast<USQGameInstance>(GetGameInstance());
	WorldManager->Seed = GameInstance->Seed;
	USQPerlinLibrary::SetSeed(WorldManager->Seed);
	MARK_PROPERTY_DIRTY_FROM_NAME(ASQWorldManager, Seed, WorldManager)
	
	WorldManager->FinishSpawning(FTransform());
}

void ASQGameState::OnRep_WorldManager() const
{
	ASQPlayerController* PlayerController = Cast<ASQPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(),0));
	PlayerController->WorldManager = WorldManager;
	USQPerlinLibrary::SetSeed(WorldManager->Seed);
	UE_LOG(LogTemp,Warning,TEXT("Seed : %i"), WorldManager->Seed);
	PlayerController->Client_Connect();
}
