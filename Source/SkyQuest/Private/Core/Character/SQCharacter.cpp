#include "Core/Character/SQCharacter.h"

#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "Core/World/SQWorldManager.h"
#include "Components/CapsuleComponent.h"
#include "Core/Gameplay/SQGameState.h"
#include "Core/Inventory/SQItemStack.h"
#include "Core/Libraries/SQInventoryLibrary.h"
#include "Core/World/SQVoxel.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Net/UnrealNetwork.h"

ASQCharacter::ASQCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(55.0f,96.0f);
	
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	CameraComponent->SetupAttachment(GetCapsuleComponent());
	CameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f));
	CameraComponent->bUsePawnControlRotation = true;
	CameraComponent->SetIsReplicated(true);

	Crafter = CreateDefaultSubobject<USQCrafter>(TEXT("Crafter"));
}

void ASQCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	PlayerController = Cast<ASQPlayerController>(Controller);

	if (GetNetMode() == NM_DedicatedServer)
	{
		Inventory = NewObject<USQInventory>(this, InventoryClass);
		Inventory->RegisterComponent();
		Inventory->SetIsReplicated(true);
	}
	
	if (PlayerController)
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(InputMappingContext, 0);
		}
	}

	WorldManager = Cast<ASQGameState>(GetWorld()->GetGameState())->WorldManager;
	FIntVector ChunkIndex = USQVoxel::GetChunkIndexFromWorld(GetActorLocation(),WorldManager->ChunkSize);
	WorldManager->AddActorInChunk(ChunkIndex,this);
}

void ASQCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASQCharacter, Inventory);
	DOREPLIFETIME(ASQCharacter, SelectedItem)
}

void ASQCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASQCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		// Jump
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ASQCharacter::StartJump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ASQCharacter::StopJumping);

		// Move
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ASQCharacter::Move);
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ASQCharacter::Look);

		// Debug
		EnhancedInputComponent->BindAction(DebugAction, ETriggerEvent::Completed, this, &ASQCharacter::Debug);

		// Sprint
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Started, this, &ASQCharacter::StartSprint);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Completed, this, &ASQCharacter::StopSprint);

		// Modify blocks
		EnhancedInputComponent->BindAction(PlaceBlockAction, ETriggerEvent::Completed, this, &ASQCharacter::PlaceBlock);
		EnhancedInputComponent->BindAction(DestroyBlockAction, ETriggerEvent::Completed, this, &ASQCharacter::DestroyBlock);

		// SelectItem
		EnhancedInputComponent->BindAction(SelectItemAction, ETriggerEvent::Triggered, this, &ASQCharacter::SelectItem);
		
		// DropItem
		EnhancedInputComponent->BindAction(DropItemAction, ETriggerEvent::Completed, this, &ASQCharacter::DropItem);
		
		// Interact
		EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Completed, this, &ASQCharacter::Interact);
	}

}

void ASQCharacter::OnRep_Inventory()
{
	OnInventoryReady();
}

void ASQCharacter::Move(const FInputActionValue& Value)
{
	if (!PlayerController->bShowMouseCursor)
	{
		const FVector2D MovementVector = Value.Get<FVector2D>();

		if (Controller != nullptr)
		{
			AddMovementInput(GetActorForwardVector(), MovementVector.Y);
			AddMovementInput(GetActorRightVector(), MovementVector.X);
		}
	}
}

void ASQCharacter::Look(const FInputActionValue& Value)
{
	if (!PlayerController->bShowMouseCursor)
	{
		const FVector2D LookAxisVector = Value.Get<FVector2D>();

		if (Controller != nullptr)
		{
			AddControllerYawInput(LookAxisVector.X);
			AddControllerPitchInput(LookAxisVector.Y);

			CameraVerticalOrientation = CameraComponent->GetForwardVector().Z;
			Server_SetCameraOrientation(CameraComponent->GetForwardVector().Z);
		}
	}
}

void ASQCharacter::Server_SetCameraOrientation_Implementation(float NewOrientation)
{
	CameraVerticalOrientation = NewOrientation;
}

void ASQCharacter::Debug(const FInputActionValue& Value)
{
	GEngine->AddOnScreenDebugMessage(-1,5,FColor::Red,TEXT("Client : Debug"));
	Server_Debug();
}

void ASQCharacter::Server_Debug_Implementation()
{
	GEngine->AddOnScreenDebugMessage(-1,5,FColor::Red,TEXT("Server : Debug"));
}

void ASQCharacter::StartSprint(const FInputActionValue& Value)
{
	if (!PlayerController->bShowMouseCursor)
	{
		Server_StartSprint();
	}
}

void ASQCharacter::StopSprint(const FInputActionValue& Value)
{
	if (!PlayerController->bShowMouseCursor)
	{
		Server_StopSprint();
	}
}

void ASQCharacter::SelectItem(const FInputActionValue& Value)
{
	if (!PlayerController->bShowMouseCursor)
	{
		Server_ChangeSelectedItem(Value.Get<float>());
	}
}

void ASQCharacter::DropItem()
{
	USQItem* ItemToDrop;
	if (Inventory->GetItem(SelectedItem,0,ItemToDrop))
	{
		Server_DropItem();
	}
}

void ASQCharacter::Server_DropItem_Implementation()
{
	USQItem* ItemToDrop;
	if (Inventory->GetItem(SelectedItem,0,ItemToDrop))
	{
		FTransform ItemTransform = CameraComponent->GetComponentTransform();
		ItemTransform.SetLocation(ItemTransform.GetLocation() + FVector(0,0,300));
		ASQItemStack* ItemStack = GetWorld()->SpawnActorDeferred<ASQItemStack>(
			WorldManager->ItemStackClass,
			ItemTransform,
			this,
			nullptr,
			ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		USQItem* Item;
		USQInventoryLibrary::CreateItem(ItemToDrop->ItemId,GetWorld()->GetGameInstance(),1,Item);
		ItemStack->Item = Item;
		ItemStack->FinishSpawning(ItemTransform);
		
		FVector CameraOrientation = CameraComponent->GetForwardVector();
		CameraOrientation.Z = CameraVerticalOrientation;
		ItemStack->StaticMeshComponent->AddImpulse(CameraOrientation * 600,NAME_None,true);

		GEngine->AddOnScreenDebugMessage(-1,5,FColor::Purple,(CameraOrientation * 600).ToString());

		ItemToDrop->Stack--;
		if (ItemToDrop->Stack <= 0)
		{
			Inventory->RemoveItemAt(SelectedItem,0,ItemToDrop);
		}
	}
}

void ASQCharacter::StartJump()
{
	if (!PlayerController->bShowMouseCursor)
	{
		Jump();
	}
}

void ASQCharacter::Server_ChangeSelectedItem_Implementation(int32 NewSelectedItem)
{
	SelectedItem = FMath::Clamp(SelectedItem + NewSelectedItem, 0, Inventory->SizeX - 1);
}

FHitResult ASQCharacter::Raycast(float Length, TEnumAsByte<EDrawDebugTrace::Type> DrawDebug)
{
	FHitResult HitResult;
	FVector CameraOrientation = CameraComponent->GetForwardVector();
	CameraOrientation.Z = CameraVerticalOrientation;
	UKismetSystemLibrary::LineTraceSingle(
		GetWorld(),
		CameraComponent->GetComponentLocation(),
		CameraComponent->GetComponentLocation() + CameraOrientation * Length,
		UEngineTypes::ConvertToTraceType(ECC_Visibility),
		false,
		{this},
		DrawDebug,
		HitResult,
		true
		);
	return HitResult;
}

void ASQCharacter::DestroyBlock()
{
	if (!PlayerController->bShowMouseCursor)
	{
		Server_DestroyBlock();
	}
}

void ASQCharacter::PlaceBlock()
{
	USQItem* Item;
	if (!PlayerController->bShowMouseCursor && Inventory->GetItem(SelectedItem,0,Item))
	{
		Server_PlaceBlock();
	}
}

void ASQCharacter::Server_PlaceBlock_Implementation()
{
	USQItem* Item;
	if (Inventory->GetItem(SelectedItem,0,Item))
	{
		FHitResult HitResult = Raycast(800.0f);

		if (HitResult.bBlockingHit)
		{
			FIntVector WorldIndex = USQVoxel::GetBlockIndexInWorld(HitResult.ImpactPoint - HitResult.Normal) + FIntVector(HitResult.Normal);

			if (WorldManager->ModifyBlock(WorldIndex, Item->GetItemData().BlockID))
			{
				Item->Stack--;
				if (Item->Stack <= 0)
				{
					Inventory->RemoveItemAt(SelectedItem,0,Item);
				}
			}
		}
	}
}

void ASQCharacter::Server_DestroyBlock_Implementation()
{
	FHitResult HitResult = Raycast(800.0f);
	
	if (HitResult.bBlockingHit)
	{		
		FIntVector WorldIndex = USQVoxel::GetBlockIndexInWorld(HitResult.ImpactPoint - HitResult.Normal);

		WorldManager->ModifyBlock(WorldIndex, 0);
	}
}

void ASQCharacter::OnRep_SelectedItem() const
{
	Inventory->OnToolbarUpdated();
}

void ASQCharacter::Client_StartSprint_Implementation()
{
	GetCharacterMovement()->MaxWalkSpeed = 2400.0f;
}

void ASQCharacter::Client_StopSprint_Implementation()
{
	GetCharacterMovement()->MaxWalkSpeed = 600.0f;
}

void ASQCharacter::Server_StartSprint_Implementation()
{
	GetCharacterMovement()->MaxWalkSpeed = 2400.0f;
	Client_StartSprint();
}

void ASQCharacter::Server_StopSprint_Implementation()
{
	GetCharacterMovement()->MaxWalkSpeed = 600.0f;
	Client_StopSprint();
}
