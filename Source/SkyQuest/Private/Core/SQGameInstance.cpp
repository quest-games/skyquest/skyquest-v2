#include "Core/SQGameInstance.h"

USQGameInstance::USQGameInstance()
{
	ReadConfigFile();
	
	const ConstructorHelpers::FObjectFinder<UDataTable> BlockTableFinder(TEXT("DataTable'/Game/DataTable/DT_Blocks.DT_Blocks'"));
	FString ContextString;
	if (BlockTableFinder.Object)
	{
		BlockTable = BlockTableFinder.Object;
		TArray<FName> RowNames = BlockTable->GetRowNames();

		for (const FName& Name : RowNames)
		{
			if (const FSQBlock* Block = BlockTable->FindRow<FSQBlock>(Name, ContextString))
			{
				Blocks.Add(*Block);
			}
		}
	}
	const ConstructorHelpers::FObjectFinder<UDataTable> ItemsTableFinder(TEXT("DataTable'/Game/DataTable/DT_Items.DT_Items'"));
	if (ItemsTableFinder.Object)
	{
		ItemTable = ItemsTableFinder.Object;
		TArray<FName> RowNames = ItemTable->GetRowNames();

		for (const FName& Name : RowNames)
		{
			if (const FSQItemData* Item = ItemTable->FindRow<FSQItemData>(Name, ContextString))
			{
				Items.Add(*Item);
			}
		}
	}
	const ConstructorHelpers::FObjectFinder<UDataTable> StructureTableFinder(TEXT("DataTable'/Game/DataTable/DT_Structures.DT_Structures'"));
	if (StructureTableFinder.Object)
	{
		StructureTable = StructureTableFinder.Object;
		TArray<FName> RowNames = StructureTable->GetRowNames();

		for (const FName& Name : RowNames)
		{
			if (const FSQWorldTemplate* Structure = StructureTable->FindRow<FSQWorldTemplate>(Name, ContextString))
			{
				Structures.Add(*Structure);
			}
		}
	}
	const ConstructorHelpers::FObjectFinder<UDataTable> RecipeTableFinder(TEXT("DataTable'/Game/DataTable/DT_Recipes.DT_Recipes'"));
	if (RecipeTableFinder.Object)
	{
		RecipeTable = RecipeTableFinder.Object;
		TArray<FName> RowNames = RecipeTable->GetRowNames();

		for (const FName& Name : RowNames)
		{
			if (const FSQRecipe* Recipe = RecipeTable->FindRow<FSQRecipe>(Name, ContextString))
			{
				Recipes.Add(*Recipe);
			}
		}
	}
}

bool USQGameInstance::GetBlock(const int32 ID, FSQBlock& Block) const
{
	if (Blocks.IsValidIndex(ID))
	{
		Block = Blocks[ID];
		return true;
	}
	return false;
}

bool USQGameInstance::GetItem(const int32 ID, FSQItemData& Item) const
{
	if (Items.IsValidIndex(ID))
	{
		Item = Items[ID];
		return true;
	}
	return false;
}

bool USQGameInstance::GetStructure(const int32 ID, FSQWorldTemplate& Structure) const
{
	if (Structures.IsValidIndex(ID))
	{
		Structure = Structures[ID];
		return true;
	}
	return false;
}

bool USQGameInstance::GetRecipe(const int32 ID, FSQRecipe& Recipe) const
{
	if (Recipes.IsValidIndex(ID))
	{
		Recipe = Recipes[ID];
		return true;
	}
	return false;
}

void USQGameInstance::ReadConfigFile()
{
	if (!GConfig) return;

	GConfig->GetInt(TEXT("/Script/SkyQuest.GameInstance"),TEXT("Seed"),Seed, GGameIni);
	GConfig->GetInt(TEXT("/Script/SkyQuest.GameInstance"),TEXT("RenderDistance"),RenderDistance, GGameIni);
}
