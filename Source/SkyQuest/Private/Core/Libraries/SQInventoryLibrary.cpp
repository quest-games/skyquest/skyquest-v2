// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Libraries/SQInventoryLibrary.h"

#include "Core/SQGameInstance.h"

bool USQInventoryLibrary::CreateItem(const int32 ItemType, UGameInstance* GameInstance,const int32 Stack, USQItem* &Item)
{
	Item = USQItem::CreateItem(ItemType, Stack, GameInstance);
	return Item != nullptr;
}

bool USQInventoryLibrary::CreateItemFromBlock(const int32 BlockType, UGameInstance* GameInstance, const int32 Stack, USQItem*& Item)
{
	FSQBlock BlockData;
	Cast<USQGameInstance>(GameInstance)->GetBlock(BlockType,BlockData);
	FSQItemData ItemData;
	Cast<USQGameInstance>(GameInstance)->GetItem(BlockData.ItemID,ItemData);
	return CreateItem(ItemData.ID,GameInstance,Stack,Item);
}
