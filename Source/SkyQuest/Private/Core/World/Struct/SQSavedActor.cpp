﻿#include "Core/World/Struct/SQSavedActor.h"

uint32 GetTypeHash(const FSQSavedActor& SavedActor)
{
	return FCrc::MemCrc32(&SavedActor, sizeof(SavedActor));
}
