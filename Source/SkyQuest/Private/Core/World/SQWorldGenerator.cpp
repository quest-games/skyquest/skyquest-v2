﻿#include "Core/World/SQWorldGenerator.h"

#include "Core/SQGameInstance.h"
#include "Core/World/SQVoxel.h"

bool USQWorldGenerator::PopulateChunk(const FIntVector& ChunkIndex, const FIntVector& InChunkSize, TMap<FIntVector, TArray<FSQBlockToModify>>& BlockToModifies, TArray<int16>& Blocks)
{
	ChunkSize = InChunkSize;
	
	Blocks.Init(0,ChunkSize.X * ChunkSize.Y * ChunkSize.Z);
	BlockToModifies = {};
	
	return true;
}

void USQWorldGenerator::AddWorldTemplate(const int16 WorldTemplateId, const FIntVector& GlobalOriginIndex,
	TMap<FIntVector, TArray<FSQBlockToModify>>& BlockToModifies) const
{
	FSQWorldTemplate WorldTemplate;
	Cast<USQGameInstance>(GetWorld()->GetGameInstance())->GetStructure(WorldTemplateId,WorldTemplate);
	for (const FSQBlockToModify& BlockToModify : WorldTemplate.BlockToModifies)
	{
		const FIntVector BlockModificationGlobalIndex = GlobalOriginIndex + BlockToModify.BlockIndex;
		FIntVector ModificationChunkIndex = USQVoxel::GetChunkIndex(BlockModificationGlobalIndex, ChunkSize);
		FSQBlockToModify LocalModification = FSQBlockToModify(BlockToModify);
		LocalModification.BlockIndex = USQVoxel::TruncWorldIndexInChunk(ChunkSize,BlockModificationGlobalIndex);
		if (TArray<FSQBlockToModify>* SavedModifications = BlockToModifies.Find(ModificationChunkIndex))
		{
			SavedModifications->Add(LocalModification);
		}
		else
		{
			BlockToModifies.Add(ModificationChunkIndex,{LocalModification});
		}
	}
}
