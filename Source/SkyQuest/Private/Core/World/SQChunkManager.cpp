#include "Core/World/SQChunkManager.h"

#include "Core/World/SQWorldManager.h"
#include "Core/SQGameInstance.h"
#include "Core/Gameplay/SQPlayerController.h"
#include "Core/World/SQVoxel.h"
#include "Core/World/Async/SQAsyncGreedyMeshing.h"
#include "Core/World/Async/SQAsyncNaiveMeshing.h"
#include "Core/World/Struct/SQChunkToShow.h"
#include "Kismet/GameplayStatics.h"

USQChunkManager::USQChunkManager()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void USQChunkManager::BeginPlay()
{
	Super::BeginPlay();

	GameInstance = Cast<USQGameInstance>(GetWorld()->GetGameInstance());
	RenderDistance = GameInstance->RenderDistance;

	WorldManager = Cast<ASQWorldManager>(GetOwner());
}

void USQChunkManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FIntVector ChunkIndex;
	while (ChunksToShow.Dequeue(ChunkIndex))
	{
		if (FSQChunkToShow** ChunkToShow = ChunksData.Find(ChunkIndex))
		{
			ShowChunk(*ChunkToShow);
			ChunksData.Remove(ChunkIndex);
		}
	}

	FSQChunkToHide ChunkToHide;
	while (ChunksToHide.Dequeue(ChunkToHide))
	{
		HideChunk(ChunkToHide);
	}
}

void USQChunkManager::SpawnChunks(const FIntVector& PlayerLocation, ASQPlayerController* PlayerController)
{
	AsyncTask(ENamedThreads::AnyHiPriThreadNormalTask, [this,PlayerLocation,PlayerController]
	{
		TSet ChunkIndices = {PlayerLocation};
		int8 SplitedRenderDistance = RenderDistance / 2;

		TArray<FIntVector> NewChunksToHide;
		LoadedChunks.GenerateKeyArray(NewChunksToHide);

		for (int X = PlayerLocation.X - SplitedRenderDistance; X < PlayerLocation.X + SplitedRenderDistance; ++X)
		{
			for (int Y = PlayerLocation.Y - SplitedRenderDistance; Y < PlayerLocation.Y + SplitedRenderDistance; ++Y)
			{
				for (int Z = PlayerLocation.Z - SplitedRenderDistance; Z < PlayerLocation.Z + SplitedRenderDistance; ++
				     Z)
				{
					const FIntVector ChunkIndex = FIntVector(X, Y, Z);
					if (FSQLoadedChunk* LoadedChunk = LoadedChunks.Find(ChunkIndex))
					{
						NewChunksToHide.Remove(ChunkIndex);
						LoadedChunk->LoadedBy.Add(PlayerController);
					}
					else
					{
						ChunkIndices.Add(ChunkIndex);
						LoadedChunks.Add(ChunkIndex, FSQLoadedChunk(nullptr, PlayerController));
					}
				}
			}
		}

		ChunkIndices.Sort([PlayerLocation](const FIntVector& A, const FIntVector& B)
		{
			return FVector::Dist(FVector(A), FVector(PlayerLocation)) < FVector::Dist(
				FVector(B), FVector(PlayerLocation));
		});

		if (GetNetMode() == NM_Client)
		{
			PlayerController->ChunksToRender = ChunkIndices;
		}
		WorldManager->PopulateChunks(ChunkIndices);
		for (const FIntVector& ChunkIndexToHide : NewChunksToHide)
		{
			ChunksToHide.Enqueue(FSQChunkToHide(ChunkIndexToHide, PlayerController));
		}
	});
}

void USQChunkManager::RenderChunks(const TSet<FIntVector>& ChunkIndices)
{
	for (const FIntVector& ChunkIndex : ChunkIndices)
	{
		const FSQWorldBlocks ChunkData = WorldManager->WorldBlocks.FindRef(ChunkIndex);
		FSQChunkToShow* ChunkToShow = new FSQChunkToShow(ChunkIndex, ChunkData.bIsAir);
		if (!ChunkData.bIsAir)
		{
			ChunksData.Add(ChunkIndex, ChunkToShow);
			FVector ChunkLocation = FVector(ChunkIndex.X * WorldManager->ChunkSize.X * 100,
			                                ChunkIndex.Y * WorldManager->ChunkSize.Y * 100,
			                                ChunkIndex.Z * WorldManager->ChunkSize.Z * 100);
			switch (MeshingType)
			{
			case Naive:
				(new FAutoDeleteAsyncTask<FSQAsyncNaiveMeshing>(ChunkData.Blocks, ChunkLocation,
				                                                WorldManager->ChunkSize,
				                                                ChunkIndex, GameInstance->Blocks,
				                                                this, ChunkToShow))->StartBackgroundTask();
				break;
			case Greedy:
				(new FAutoDeleteAsyncTask<FSQAsyncGreedyMeshing>(ChunkData.Blocks, ChunkLocation,
				                                                 WorldManager->ChunkSize,
				                                                 ChunkIndex, GameInstance->Blocks,
				                                                 this, ChunkToShow))->StartBackgroundTask();
				break;
			default:
				GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red,TEXT("INVALID MESHING TYPE"));
				break;
			}
		}
		else
		{
			AsyncTask(ENamedThreads::GameThread, [this, ChunkToShow]
			{
				ShowChunk(ChunkToShow);
			});
		}
	}
}

int32 USQChunkManager::GetBlock(const FIntVector& Index, const TArray<int16>& BlockList) const
{
	if (Index.X >= WorldManager->ChunkSize.X || Index.Y >= WorldManager->ChunkSize.Y ||
		Index.Z >= WorldManager->ChunkSize.Z || Index.X < 0 || Index.Y < 0 || Index.Z < 0)
		return 0;
	return BlockList[USQVoxel::GetBlockIndexInChunk(WorldManager->ChunkSize, Index)];
}

void USQChunkManager::ShowChunk(const FSQChunkToShow* ChunkToShow)
{
	if (FSQLoadedChunk* LoadedChunk = LoadedChunks.Find(ChunkToShow->ChunkIndex))
	{
		if (!ChunkToShow->bIsAir)
		{
			if (!LoadedChunk->Mesh)
			{
				UProceduralMeshComponent* Mesh;
				if (!FreeChunks.Dequeue(Mesh))
				{
					Mesh = NewObject<UProceduralMeshComponent>(this);
					Mesh->SetIsReplicated(false);
					Mesh->RegisterComponent();
				}

				LoadedChunk->Mesh = Mesh;
			}

			LoadedChunk->Mesh->CreateMeshSection(
				0,
				ChunkToShow->Vertices,
				ChunkToShow->Triangles,
				ChunkToShow->Normals,
				ChunkToShow->UVs,
				ChunkToShow->Colors,
				{},
				true
			);
			LoadedChunk->Mesh->SetMaterial(0, ChunkMaterialInstance);
		}

		if (FSQSavedChunk* SavedChunk = WorldManager->SavedChunks.Find(ChunkToShow->ChunkIndex))
		{
			for (const FSQSavedActor& SavedActor : SavedChunk->Actors)
			{
				SavedActor.Actor->SetActorHiddenInGame(false);
				SavedActor.Actor->SetActorTransform(SavedActor.ActorTransform);
			}

			LoadedChunk->Actors = SavedChunk->Actors;
		}
	}

	if (ChunkToShow->ChunkIndex == FIntVector::ZeroValue)
	{
		UE_LOG(LogTemp, Warning, TEXT("Chunks Rendered"))
		if (GetNetMode() == NM_Client)
		{
			ASQPlayerController* PlayerController = Cast<ASQPlayerController>(
				UGameplayStatics::GetPlayerController(GetWorld(), 0));
			if (!PlayerController->GetPawn())
			{
				PlayerController->SpawnPlayer();
			}
		}
	}

	delete ChunkToShow;
}

void USQChunkManager::HideChunk(const FSQChunkToHide& ChunkToHide)
{
	if (FSQLoadedChunk* LoadedChunk = LoadedChunks.Find(ChunkToHide.ChunkIndex))
	{
		LoadedChunk->LoadedBy.Remove(ChunkToHide.PlayerController);
		if (LoadedChunk->LoadedBy.IsEmpty())
		{
			TArray<FSQSavedActor> InvalidActors = {};
			for (FSQSavedActor& SavedActor : LoadedChunk->Actors)
			{
				if (IsValid(SavedActor.Actor))
				{
					SavedActor.Actor->SetActorHiddenInGame(true);
					SavedActor.ActorTransform = SavedActor.Actor->GetActorTransform();
				}
				else
				{
					InvalidActors.Add(SavedActor);
				}
			}
			for (const FSQSavedActor& InvalidActor : InvalidActors)
			{
				LoadedChunk->Actors.Remove(InvalidActor);
			}

			if (!LoadedChunk->Actors.IsEmpty())
			{
				if (FSQSavedChunk* SavedChunk = WorldManager->SavedChunks.Find(ChunkToHide.ChunkIndex))
				{
					SavedChunk->Actors = LoadedChunk->Actors;
				}
				else
				{
					FSQSavedChunk NewSavedChunk = FSQSavedChunk();
					NewSavedChunk.Actors = LoadedChunk->Actors;
					NewSavedChunk.ChunkIndex = ChunkToHide.ChunkIndex;
					WorldManager->SavedChunks.Add(ChunkToHide.ChunkIndex, NewSavedChunk);
				}
			}


			if (LoadedChunk->Mesh)
			{
				LoadedChunk->Mesh->ClearMeshSection(0);
				FreeChunks.Enqueue(LoadedChunk->Mesh);
			}
			LoadedChunks.Remove(ChunkToHide.ChunkIndex);
		}
	}
}
