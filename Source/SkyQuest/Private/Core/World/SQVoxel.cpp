#include "Core/World/SQVoxel.h"

FIntVector USQVoxel::GetChunkIndexFromWorld(FVector WorldLocation, const FIntVector& ChunkSize)
{
	if (WorldLocation.X < -1)
		WorldLocation.X++;
	if (WorldLocation.Y < -1)
		WorldLocation.Y++;
	if (WorldLocation.Z < -1)
		WorldLocation.Z++;
	FIntVector ChunkIndex = FIntVector(
		FMath::TruncToFloat(WorldLocation.X / 100 / ChunkSize.X),
		FMath::TruncToFloat(WorldLocation.Y / 100 / ChunkSize.Y),
		FMath::TruncToFloat(WorldLocation.Z / 100 / ChunkSize.Z)
		);
	if(WorldLocation.X < 0)
		ChunkIndex.X--;
	if(WorldLocation.Y < 0)
		ChunkIndex.Y--;
	if(WorldLocation.Z < 0)
		ChunkIndex.Z--;
	return ChunkIndex;
}

FIntVector USQVoxel::GetChunkIndex(FIntVector WorldIndex, const FIntVector& ChunkSize)
{
	if (WorldIndex.X < -1)
		WorldIndex.X++;
	if (WorldIndex.Y < -1)
		WorldIndex.Y++;
	if (WorldIndex.Z < -1)
		WorldIndex.Z++;
	FIntVector ChunkIndex = FIntVector(
		WorldIndex.X / ChunkSize.X,
		WorldIndex.Y / ChunkSize.Y,
		WorldIndex.Z / ChunkSize.Z
		);
	if(WorldIndex.X < 0)
		ChunkIndex.X--;
	if(WorldIndex.Y < 0)
		ChunkIndex.Y--;
	if(WorldIndex.Z < 0)
		ChunkIndex.Z--;
	return ChunkIndex;
}

FIntVector USQVoxel::TruncWorldIndexInChunk(const FIntVector& ChunkSize, const FIntVector& WorldIndex)
{
	FIntVector BlockIndex = FIntVector(
		fmod(WorldIndex.X + 0.0f, ChunkSize.X + 0.0f),
		fmod(WorldIndex.Y + 0.0f, ChunkSize.Y + 0.0f),
		fmod(WorldIndex.Z + 0.0f, ChunkSize.Z + 0.0f)
		);

	if (WorldIndex.X < 0 && BlockIndex.X != 0)
		BlockIndex.X += ChunkSize.X;
	if (WorldIndex.Y < 0 && BlockIndex.Y != 0)
		BlockIndex.Y += ChunkSize.Y;
	if (WorldIndex.Z < 0 && BlockIndex.Z != 0)
		BlockIndex.Z += ChunkSize.Z;

	return BlockIndex;
}

int32 USQVoxel::GetBlockIndexInChunk(const FIntVector& ChunkSize, const FIntVector& BlockIndex)
{
	return BlockIndex.Z * ChunkSize.X * ChunkSize.Y + BlockIndex.Y * ChunkSize.X + BlockIndex.X; 
}

FIntVector USQVoxel::GetBlockIndexInWorld(const FVector& WorldLocation)
{
	FIntVector BlockIndex =  FIntVector(
		FMath::TruncToInt(WorldLocation.X / 100),
		FMath::TruncToInt(WorldLocation.Y / 100),
		FMath::TruncToInt(WorldLocation.Z / 100)
	);
	
	if(WorldLocation.X < 0)
		BlockIndex.X--;
	if(WorldLocation.Y < 0)
		BlockIndex.Y--;
	if(WorldLocation.Z < 0)
		BlockIndex.Z--;

	return BlockIndex;
}

FIntVector USQVoxel::GetBlockPos(int32 BlockIndex, const FIntVector& ChunkSize)
{
	const int z = BlockIndex / (ChunkSize.X * ChunkSize.Y);
	BlockIndex -= (z * ChunkSize.X * ChunkSize.Y);
	const int y = BlockIndex / ChunkSize.X;
	const int x = BlockIndex % ChunkSize.X;
	return FIntVector(x,y,z);
}
