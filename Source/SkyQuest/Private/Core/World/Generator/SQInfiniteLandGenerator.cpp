﻿#include "Core/World/Generator/SQInfiniteLandGenerator.h"

#include "Core/SQGameInstance.h"
#include "Core/Libraries/SQPerlinLibrary.h"
#include "Core/World/SQVoxel.h"

bool USQInfiniteLandGenerator::PopulateChunk(const FIntVector& ChunkIndex, const FIntVector& InChunkSize, TMap<FIntVector, TArray<FSQBlockToModify>>& BlockToModifies, TArray<int16>& Blocks)
{
	ChunkSize = InChunkSize;
	Blocks.SetNum(ChunkSize.X * ChunkSize.Y * ChunkSize.Z);
	BlockToModifies = {};

	bool bIsAir = true;
	
	for (int16 x = 0; x < ChunkSize.X; ++x)
	{
		const int16 GlobalX = x - 1 + ChunkIndex.X * ChunkSize.X;
		for (int16 y = 0; y < ChunkSize.Y; ++y)
		{
			const int16 GlobalY = y - 1 + ChunkIndex.Y * ChunkSize.Y;
			const int16 Perlin = FMath::RoundToInt(USQPerlinLibrary::TwoD_Perlin_Fractal(GlobalX,GlobalY,Levels,Scale,Amplitude,ScaleFade,AmpFade));
			
			const float ThreePerlin = USQPerlinLibrary::TwoD_Perlin_Noise(GlobalX,GlobalY,8,10);
			const float ThreePerlinPlacement = USQPerlinLibrary::TwoD_Perlin_Noise(GlobalX,GlobalY,2,20);
			
			for (int16 z = 0; z < ChunkSize.Z; ++z)
			{
				const int16 GlobalZ = z - 1 + ChunkIndex.Z * ChunkSize.Z;
				const int32 BlockIndex = USQVoxel::GetBlockIndexInChunk(ChunkSize,FIntVector(x,y,z));
				
				const int32 DownBlock = USQVoxel::GetBlockIndexInChunk(ChunkSize, FIntVector(x,y,z-1));
			
				if (GlobalZ == Perlin + 1 && ThreePerlin > 0 && ThreePerlinPlacement > 8.5 && DownBlock >= 0 && Blocks[DownBlock] == 3)
				{
					const int16 Structure = FMath::RandRange(0,4);
					AddWorldTemplate(Structure,FIntVector(GlobalX,GlobalY,GlobalZ),BlockToModifies );
					if (bIsAir) bIsAir = false;
				}
				else if (GlobalZ == Perlin)
				{
					Blocks[BlockIndex] = 3;
					if (bIsAir) bIsAir = false;
				}
				else if (GlobalZ < Perlin && GlobalZ >= Perlin - 4)
				{
					Blocks[BlockIndex] = 2;
					if (bIsAir) bIsAir = false;
				}
				else if (GlobalZ < Perlin - 4)
				{
					Blocks[BlockIndex] = 1;
					if (bIsAir) bIsAir = false;
				}
				else
					Blocks[BlockIndex] = 0;

			}
		}
	}
	
	return bIsAir;
}
