﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/World/Generator/SQFlatLand.h"

#include "Core/World/SQVoxel.h"

bool USQFlatLand::PopulateChunk(const FIntVector& ChunkIndex, const FIntVector& InChunkSize,
                                         TMap<FIntVector, TArray<FSQBlockToModify>>& BlockToModifies, TArray<int16>& Blocks)
{
	ChunkSize = InChunkSize;
	Blocks.SetNum(ChunkSize.X * ChunkSize.Y * ChunkSize.Z);
	BlockToModifies = {};
	bool bIsAir = true;
	
	for (int32 x = 0; x < ChunkSize.X; ++x)
	{
		for (int32 y = 0; y < ChunkSize.Y; ++y)
		{
			for (int32 z = 0; z < ChunkSize.Z; ++z)
			{
				const int32 GlobalZ = z - 1 + ChunkIndex.Z * ChunkSize.Z;
				const int32 BlockIndex = USQVoxel::GetBlockIndexInChunk(ChunkSize,FIntVector(x,y,z));

				if (GlobalZ > 0)
				{
					Blocks[BlockIndex] = 0;
				}
				else if (GlobalZ == 0)
				{
					Blocks[BlockIndex] = 3;
					if (bIsAir) bIsAir = false;
				}
				else if (GlobalZ > -4)
				{
					Blocks[BlockIndex] = 2;
					if (bIsAir) bIsAir = false;
				}
				else
				{
					Blocks[BlockIndex] = 1;
					if (bIsAir) bIsAir = false;
				}
			}
		}
	}
	
	return bIsAir;
}
