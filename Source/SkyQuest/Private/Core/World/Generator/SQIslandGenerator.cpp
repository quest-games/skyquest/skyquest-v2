﻿#include "Core/World/Generator/SQIslandGenerator.h"

#include "Core/Libraries/SQPerlinLibrary.h"
#include "Core/World/SQVoxel.h"

USQIslandGenerator::USQIslandGenerator()
{
	const ConstructorHelpers::FObjectFinder<UCurveFloat> CurveFinder(TEXT("CurveFloat'/Game/Blueprint/Core/World/C_IslandPerlin.C_IslandPerlin'"));
	if (CurveFinder.Object)
	{
		PerlinCurve = CurveFinder.Object;
	}
}

bool USQIslandGenerator::PopulateChunk(const FIntVector& ChunkIndex, const FIntVector& InChunkSize, TMap<FIntVector, TArray<FSQBlockToModify>>& BlockToModifies, TArray<int16>& Blocks)
{
	ChunkSize = InChunkSize;
	Blocks.SetNum(ChunkSize.X * ChunkSize.Y * ChunkSize.Z);
	BlockToModifies = {};
	bool bIsAir = true;	
	
	bool bGenerateTower = false;
	FIntPoint TowerLocation = FIntPoint::ZeroValue;

	for (const FIntVector& TowerChunk : TowersChunks)
		if (ChunkIndex == TowerChunk)
		{
			bGenerateTower = true;
			TowerLocation = FIntPoint(
				FMath::RandRange(0,ChunkSize.X - 1),
				FMath::RandRange(0,ChunkSize.Y - 1)
			);
		}
	
	for (int32 x = 0; x < ChunkSize.X; ++x)
	{
		const int16 GlobalX = x + ChunkIndex.X * ChunkSize.X;
		for (int32 y = 0; y < ChunkSize.Y; ++y)
		{
			const int16 GlobalY = y + ChunkIndex.Y * ChunkSize.Y;

			const float PerlinZ = USQPerlinLibrary::TwoD_Perlin_Fractal(GlobalX,GlobalY,1,30,10,2,20);

			const float DX = (FMath::Abs(GlobalX) + 0.0f) / IslandSize * 100;
			const float DY = (FMath::Abs(GlobalY) + 0.0f) / IslandSize * 100;

			const float CurveX = PerlinCurve->GetFloatValue(DX);
			const float CurveY = PerlinCurve->GetFloatValue(DY);

			const int32 MaxZ = FMath::RoundToInt(PerlinZ + CurveX + CurveY);
			const int32 MinZ = FMath::RoundToInt(PerlinZ * 2.5 - CurveX * 2.5 - CurveY * 2.5 - 1);
			
			const float ThreePerlin = USQPerlinLibrary::TwoD_Perlin_Noise(GlobalX,GlobalY,8,10);
			const float ThreePerlinPlacement = USQPerlinLibrary::TwoD_Perlin_Noise(GlobalX,GlobalY,2,20);
			
			for (int32 z = 0; z < ChunkSize.Z; ++z)
			{
				const int16 GlobalZ = z + ChunkIndex.Z * ChunkSize.Z;
				const int16 BlockIndex = USQVoxel::GetBlockIndexInChunk(ChunkSize,FIntVector(x,y,z));
				if (GlobalZ >= MinZ && GlobalZ <= MaxZ)
				{
					if (GlobalZ == MaxZ)
					{
						const int32 DownBlock = USQVoxel::GetBlockIndexInChunk(ChunkSize,FIntVector(x,y,z > 0 ? z - 1 : 0));
						if (ThreePerlin > 0 && ThreePerlinPlacement > 8.5 && Blocks[DownBlock] == 3)
						{
							const int32 Structure = FMath::RandRange(0,3);
							AddWorldTemplate(Structure,FIntVector(GlobalX,GlobalY,GlobalZ),BlockToModifies);
							if (bIsAir) bIsAir = false;
						}
						else Blocks[BlockIndex] = 0;
					}
					else if (GlobalZ == MaxZ - 1)
					{
						Blocks[BlockIndex] = 3;
						if (bIsAir) bIsAir = false;
					}
					else if (GlobalZ > MaxZ - 4)
					{
						Blocks[BlockIndex] = 2;
						if (bIsAir) bIsAir = false;
					}
					else
					{
						Blocks[BlockIndex] = 1;
						if (bIsAir) bIsAir = false;
					}
				}
				else
					Blocks[BlockIndex] = 0;
			}

			if (bGenerateTower && x == TowerLocation.X && y == TowerLocation.Y)
			{
				AddWorldTemplate(4,FIntVector(GlobalX,GlobalY,MaxZ - 4),BlockToModifies);
			}
		}
	}
	
	return bIsAir;
}
