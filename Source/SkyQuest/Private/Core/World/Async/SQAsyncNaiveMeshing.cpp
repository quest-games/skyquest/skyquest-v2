﻿#include "Core/World/Async/SQAsyncNaiveMeshing.h"

#include "Core/World/SQChunkManager.h"
#include "Core/World/Struct/SQBlock.h"

void FSQAsyncNaiveMeshing::DoWork()
{
	for (int X = 1; X <= ChunkSize.X; ++X)
	{
		for (int Y = 1; Y <= ChunkSize.Y; ++Y)
		{
			for (int Z = 1; Z <= ChunkSize.Z; ++Z)
			{
				FIntVector BlockIndex = FIntVector(X, Y, Z);
				const int16 BlockId = GetBlock(BlockIndex);
				if (BlockId != 0)
				{
					for (auto Direction : {
						Forward,
						Right,
						Back,
						Left,
						Up,
						Down
					})
					{
						FIntVector NextBlockIndex = BlockIndex + FIntVector(Neighbours[Direction]);
						if (IsAir(NextBlockIndex))
						{
							CreateFace(BlockIndex, Direction, BlockId);
						}
					}
				}
			}
		}
	}

	ChunkToShow->bIsAir = ChunkToShow->Vertices.Num() == 0;

	ChunkManager->ChunksToShow.Enqueue(ChunkIndex);
}

void FSQAsyncNaiveMeshing::CreateFace(const FIntVector& BlockIndex,
                                      ESQDirection Direction, int16 BlockId)
{
	const FVector Normal = Neighbours[Direction];
	
	int32 DirectionOffset;
	if (Normal.X == 1)
		DirectionOffset = 0;

	else if (Normal.X == -1)
		DirectionOffset = 1;

	else if (Normal.Y == 1)
		DirectionOffset = 2;

	else if (Normal.Y == -1)
		DirectionOffset = 3;

	else if (Normal.Z == 1)
		DirectionOffset = 4;

	else
		DirectionOffset = 5;

	const FColor VertexColor = FColor(0, 0, 0, Blocks[BlockId].TextureIndexes[DirectionOffset]);

	TArray TexCoords{
		FVector2D(0, 0),
		FVector2D(1, 0),
		FVector2D(1, 1),
		FVector2D(0, 1),
	};

	const FVector BlockLocation = ChunkLocation + FVector(BlockIndex) * 100;

	const TArray<FVector> VerticesPositions = GetFaceVertices(Direction,BlockLocation);

	TArray<int32> Vertices = {};
	for (int i = 0; i < 4; ++i)
	{
		ChunkToShow->Vertices.Add(
			BlockVertexData[BlockTriangleData[i + static_cast<int>(Direction) * 4]] + BlockLocation
		);
		ChunkToShow->Normals.Add(Normal);
		ChunkToShow->UVs.Add(TexCoords[i]);
		ChunkToShow->Colors.Add(VertexColor);
	}
	
	ChunkToShow->Triangles.Append({VertexCount + 2, VertexCount + 1, VertexCount});
	ChunkToShow->Triangles.Append({VertexCount + 3, VertexCount + 2, VertexCount});
	
	VertexCount += 4;
}

TArray<FVector> FSQAsyncNaiveMeshing::GetFaceVertices(ESQDirection Direction, const FVector& Position) const
{
	TArray<FVector> Vertices;

	for (int i = 0; i < 4; i++)
	{
		Vertices.Add(BlockVertexData[BlockTriangleData[i + static_cast<int>(Direction) * 4]] + Position);
	}
	
	return Vertices;
}

