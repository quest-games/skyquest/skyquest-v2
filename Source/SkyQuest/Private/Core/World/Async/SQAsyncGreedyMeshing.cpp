﻿#include "Core/World/Async/SQAsyncGreedyMeshing.h"

#include "Core/World/SQChunkManager.h"
#include "Core/World/SQVoxel.h"
#include "Core/World/Struct/SQBlock.h"
#include "Core/World/Struct/SQMask.h"

void FSQAsyncGreedyMeshing::DoWork()
{
	TArray<FSQMask> Masks = {};
		
	// Sweep over each axis (X, Y, Z)
	for (int Axis = 0; Axis < 3; ++Axis)
	{
		// 2 Perpendicular axis
		const int Axis1 = (Axis + 1) % 3;
		const int Axis2 = (Axis + 2) % 3;

		const int MainAxisLimit = ChunkSize[Axis];
		const int Axis1Limit = ChunkSize[Axis1];
		const int Axis2Limit = ChunkSize[Axis2];

		FIntVector DeltaAxis1 = FIntVector::ZeroValue;
		FIntVector DeltaAxis2 = FIntVector::ZeroValue;

		FIntVector ChunkItr = FIntVector::ZeroValue;
		FIntVector AxisMask = FIntVector::ZeroValue;

		AxisMask[Axis] = 1;
		
		Masks.SetNum(Axis1Limit * Axis2Limit);

		// Check each slice of the chunk
		for (ChunkItr[Axis] = -1; ChunkItr[Axis] < MainAxisLimit;)
		{
			int N = 0;

			// Compute Mask
			for (ChunkItr[Axis2] = 0; ChunkItr[Axis2] < Axis2Limit; ++ChunkItr[Axis2])
			{
				for (ChunkItr[Axis1] = 0; ChunkItr[Axis1] < Axis1Limit; ++ChunkItr[Axis1])
				{
					const int32 CurrentBlock = GetBlock(ChunkItr);
					const int32 CompareBlock = GetBlock(ChunkItr + AxisMask);

					const bool CurrentBlockOpaque = CurrentBlock != 0;
					const bool CompareBlockOpaque = CompareBlock != 0;

					if (CurrentBlockOpaque == CompareBlockOpaque)
					{
						Masks[N++] = FSQMask{-1, 0};
					}
					else if (CurrentBlockOpaque)
					{
						Masks[N++] = FSQMask{CurrentBlock, 1};
					}
					else
					{
						Masks[N++] = FSQMask{CompareBlock, -1};
					}
				}
			}

			++ChunkItr[Axis];
			N = 0;

			// Generate Mesh From Mask
			for (int j = 0; j < Axis2Limit; ++j)
			{
				for (int i = 0; i < Axis1Limit;)
				{
					if (Masks[N].Normal != 0)
					{
						const auto CurrentMask = Masks[N];
						ChunkItr[Axis1] = i;
						ChunkItr[Axis2] = j;

						int Width;

						for (Width = 1; i + Width < Axis1Limit && CompareMask(Masks[N + Width], CurrentMask); ++Width)
						{
						}

						int Height;
						bool Done = false;

						for (Height = 1; j + Height < Axis2Limit; ++Height)
						{
							for (int k = 0; k < Width; ++k)
							{
								if (CompareMask(Masks[N + k + Height * Axis1Limit], CurrentMask)) continue;

								Done = true;
								break;
							}

							if (Done) break;
						}

						DeltaAxis1[Axis1] = Width;
						DeltaAxis2[Axis2] = Height;

						CreateQuad(
							CurrentMask,
							AxisMask,
							Width,
							Height,
							ChunkItr,
							ChunkItr + DeltaAxis1,
							ChunkItr + DeltaAxis2,
							ChunkItr + DeltaAxis1 + DeltaAxis2
						);

						DeltaAxis1 = FIntVector::ZeroValue;
						DeltaAxis2 = FIntVector::ZeroValue;

						for (int l = 0; l < Height; ++l)
						{
							for (int k = 0; k < Width; ++k)
							{
								Masks[N + k + l * Axis1Limit] = FSQMask{-1, 0};
							}
						}

						i += Width;
						N += Width;
					}
					else
					{
						i++;
						N++;
					}
				}
			}
		}
	}

	ChunkToShow->bIsAir = ChunkToShow->Vertices.Num() == 0;
	
	ChunkManager->ChunksToShow.Enqueue(ChunkIndex);
}

bool FSQAsyncGreedyMeshing::CompareMask(const FSQMask M1, const FSQMask M2)
{
	return M1.Block == M2.Block && M1.Normal == M2.Normal;
}

void FSQAsyncGreedyMeshing::CreateQuad(const FSQMask& Mask,
	const FIntVector& AxisMask, const int Width, const int Height, const FIntVector& V1, const FIntVector& V2,
	const FIntVector& V3, const FIntVector& V4)
{
	const FVector Normal = FVector(AxisMask * Mask.Normal);

	int32 DirectionOffset;
	if (Normal.X == 1)
		DirectionOffset = 0;
	
	else if (Normal.X == -1)
		DirectionOffset = 1;
	
	else if (Normal.Y == 1)
		DirectionOffset = 2;
	
	else if (Normal.Y == -1)
		DirectionOffset = 3;
	
	else if (Normal.Z == 1)
		DirectionOffset = 4;
	
	else
		DirectionOffset = 5;
	
	FSQBlock BlockData = Blocks[Mask.Block];
	const FColor VertexColor = FColor(0,0,0,BlockData.TextureIndexes[DirectionOffset]);

	ChunkToShow->Vertices.Append({
		FVector(V1) * 100 + ChunkLocation,
		FVector(V2) * 100 + ChunkLocation,
		FVector(V3) * 100 + ChunkLocation,
		FVector(V4) * 100 + ChunkLocation
	});

	ChunkToShow->Triangles.Append({
		VertexCount,
		VertexCount + 2 + Mask.Normal,
		VertexCount + 2 - Mask.Normal,
		VertexCount + 3,
		VertexCount + 1 - Mask.Normal,
		VertexCount + 1 + Mask.Normal
	});

	ChunkToShow->Normals.Append({
		Normal,
		Normal,
		Normal,
		Normal
	});

	ChunkToShow->Colors.Append({
		VertexColor,
		VertexColor,
		VertexColor,
		VertexColor
	});

	if (Normal.X == 1 || Normal.X == -1)
	{
		ChunkToShow->UVs.Append({
			FVector2D(Width, Height),
			FVector2D(0, Height),
			FVector2D(Width, 0),
			FVector2D(0, 0),
		});
	}
	else
	{
		ChunkToShow->UVs.Append({
			FVector2D(Height, Width),
			FVector2D(Height, 0),
			FVector2D(0, Width),
			FVector2D(0, 0),
		});
	}

	VertexCount += 4;
}

int32 FSQAsyncGreedyMeshing::GetBlock(const FIntVector& Index) const
{
	if (Index.X >= ChunkSize.X || Index.Y >= ChunkSize.Y ||
		Index.Z >= ChunkSize.Z || Index.X < 0 || Index.Y < 0 || Index.Z < 0)
		return 0;

	return BlockList[USQVoxel::GetBlockIndexInChunk(ChunkSize, Index)];
}
