#include "Core/World/SQWorldManager.h"

#include "Core/Gameplay/SQPlayerController.h"
#include "Core/Inventory/SQItemStack.h"
#include "Core/Libraries/SQInventoryLibrary.h"
#include "Core/World/SQChunkManager.h"
#include "Kismet/GameplayStatics.h"
#include "Core/World/SQVoxel.h"
#include "Core/World/SQWorldGenerator.h"
#include "Core/World/Generator/SQInfiniteLandGenerator.h"
#include "Net/UnrealNetwork.h"

ASQWorldManager::ASQWorldManager()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	
	ChunkManager = CreateDefaultSubobject<USQChunkManager>(TEXT("ChunkManager"));

	WorldGenerator = CreateDefaultSubobject<USQInfiniteLandGenerator>(TEXT("WorldGenerator"));

	bReplicates = true;
	bAlwaysRelevant = true;
}

void ASQWorldManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	FDoRepLifetimeParams Params;
	Params.bIsPushBased = true;
	DOREPLIFETIME_WITH_PARAMS_FAST(ASQWorldManager, Seed, Params)
}

void ASQWorldManager::BeginPlay()
{
	Super::BeginPlay();

	if (WorldGeneratorClass)
	{
		WorldGenerator = NewObject<USQWorldGenerator>(this, WorldGeneratorClass);
	}

	GameInstance = Cast<USQGameInstance>(GetWorld()->GetGameInstance());

	UE_LOG(LogTemp,Warning,TEXT("WorldManager Spawned"))
}

void ASQWorldManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASQWorldManager::PopulateChunks(const TSet<FIntVector>& ChunkIndices)
{
	bIsPopulating = true;
	double Start = FPlatformTime::Seconds();
	FScopeLock Lock(&DataGuard);
	for (const FIntVector& ChunkIndex : ChunkIndices)
	{
		if (!WorldBlocks.Contains(ChunkIndex))
		{
			PopulateChunk(ChunkIndex);
		}
	}

	for (const FIntVector& ChunkIndex : ChunkIndices)
	{
		TArray<FSQBlockToModify>* Modifications = WorldBlockToModifies.Find(ChunkIndex);
		FSQWorldBlocks* ChunkBlocks = WorldBlocks.Find(ChunkIndex);
		if (Modifications && ChunkBlocks)
		{
			for (const FSQBlockToModify& Modification : *Modifications)
			{
				int32 BlockIndex = USQVoxel::GetBlockIndexInChunk(ChunkSize,Modification.BlockIndex);
				if (Modification.bIsImportant || ChunkBlocks->Blocks[BlockIndex] == 0)
				{
					ChunkBlocks->Blocks[BlockIndex] = Modification.BlockType;
				}
			} 
		}
	}
	
	bIsPopulating = false;

	double End = FPlatformTime::Seconds();
	UE_LOG(LogTemp,Warning,TEXT("Chunks Populated in %f s"), End-Start)
	if (GetNetMode() == NM_DedicatedServer)
	{
		ApplyModifications(ChunkIndices);
		
		UE_LOG(LogTemp,Warning,TEXT("Render %i chunks"), ChunkIndices.Num())
		ChunkManager->RenderChunks(ChunkIndices);
	}
	else
	{
		AsyncTask(ENamedThreads::GameThread, [this, ChunkIndices = MakeShared<TArray<FIntVector>>(ChunkIndices.Array())]
		{
			ASQPlayerController* PlayerController = Cast<ASQPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(),0));
			PlayerController->Client_FetchChunks(ChunkIndices.Get());
		});	
	}
}

void ASQWorldManager::PopulateChunk(const FIntVector& ChunkIndex)
{
	TMap<FIntVector, TArray<FSQBlockToModify>> BlockToModifies;
	TArray<int16> Blocks;
	const bool bIsAir = WorldGenerator->PopulateChunk(ChunkIndex,ChunkSize,BlockToModifies,Blocks);
	for (const TPair<FIntVector, TArray<FSQBlockToModify>>& Pair : BlockToModifies)
	{
		if (TArray<FSQBlockToModify>* Modifications = WorldBlockToModifies.Find(Pair.Key))
		{
			Modifications->Append(Pair.Value);
		}
		else
		{
			WorldBlockToModifies.Add(Pair);
		}
	}
	WorldBlocks.Add(ChunkIndex,FSQWorldBlocks(Blocks,bIsAir));
}

void ASQWorldManager::ApplyModifications(const FIntVector& ChunkIndex)
{
	FSQSavedChunk* SavedChunk = SavedChunks.Find(ChunkIndex);
	FSQWorldBlocks* Blocks = WorldBlocks.Find(ChunkIndex);
	if (SavedChunk && Blocks)
	{
		if (SavedChunk->ModifiedChunk.Modifications.Num() > 0)
		{
			Blocks->bIsAir = false;
		}
		for (const FSQModifiedChunkEntry& Entry : SavedChunk->ModifiedChunk.Modifications)
		{
			Blocks->Blocks[Entry.Position] = Entry.BlockId;
		}
	}
}

void ASQWorldManager::ApplyModifications(const TSet<FIntVector>& ChunkIndices)
{
	for (const FIntVector& ChunkIndex : ChunkIndices)
	{
		ApplyModifications(ChunkIndex);
	} 
}

void ASQWorldManager::ConnectPlayer(ASQPlayerController* PlayerController)
{
	ChunkManager->SpawnChunks(PlayerController->PlayerLocation, PlayerController);
	
	if (!ChunkManager->LoadedChunks.Contains(PlayerController->PlayerLocation))
	{
		PendingConnections.Add(PlayerController);
	}

	PlayerController->Client_SpawnChunks();
}

FIntVector ASQWorldManager::GetSpawnableLocation(const FIntVector& ChunkIndex)
{
	FSQWorldBlocks* Blocks = WorldBlocks.Find(ChunkIndex);
	TArray<int32> SpawnableBlocks = {};
	
	for (int32 i = 0; i < Blocks->Blocks.Num(); i++)
	{
		if (Blocks->Blocks[i] == 3)
		{
			SpawnableBlocks.Add(i);
		}
	}

	if (!SpawnableBlocks.IsEmpty())
	{
		const int32 SelectedBlock = SpawnableBlocks[FMath::RandRange(0,SpawnableBlocks.Num() - 1)];
		return USQVoxel::GetBlockPos(SelectedBlock,ChunkSize) + FIntVector(0,0,2);
	}

	return ChunkSize / 2;
}

FSQBlock ASQWorldManager::GetWorldBlock(const FVector& WorldLocation) const
{
	int16 BlockID = 0;
	
	const FIntVector ChunkIndex = USQVoxel::GetChunkIndexFromWorld(WorldLocation,ChunkSize);
	
	const FVector ChunkLocation = FVector(ChunkIndex * ChunkSize * 100);

	const FIntVector BlockLocation = FIntVector(FVector(ChunkLocation - WorldLocation).GetAbs() / 100);

	const int32 BlockIndex = USQVoxel::GetBlockIndexInChunk(ChunkSize,BlockLocation);
	
	if (const FSQWorldBlocks* BlockList = WorldBlocks.Find(ChunkIndex))
	{
		BlockID = BlockList->Blocks[BlockIndex];
	}
	
	return GameInstance->Blocks[BlockID];
}

bool ASQWorldManager::ModifyBlock(const FIntVector& WorldIndex, int16 NewBlock, int16& OldBlock)
{
	const FIntVector ChunkIndex = USQVoxel::GetChunkIndex(WorldIndex,ChunkSize);

	const FIntVector BlockIndex = USQVoxel::TruncWorldIndexInChunk(ChunkSize, WorldIndex);

	const int16 Index = USQVoxel::GetBlockIndexInChunk(ChunkSize,BlockIndex);
	
	FSQWorldBlocks* BlockList = WorldBlocks.Find(ChunkIndex);
	
	FSQBlock NewBlockData;
	if (BlockList && GameInstance->GetBlock(NewBlock,NewBlockData))
	{
		FVector BlockLocation = FVector(WorldIndex) * 100 + 50;
		
		OldBlock = BlockList->Blocks[Index];

		FSQBlock OldBlockData;
		if (!GameInstance->GetBlock(OldBlock,OldBlockData))
		{
			return false;
		}

		if (NewBlock == OldBlock)
		{
			return false;
		}
		
		if (!NewBlockData.bPlaceableWithCollision)
		{
			FHitResult HitResult;
			UKismetSystemLibrary::BoxTraceSingle(
				GetWorld(),
				BlockLocation,
				BlockLocation,
				FVector(49.0f),
				FRotator(0.0f),
				UEngineTypes::ConvertToTraceType(ECC_Visibility),
				false,
				{},
				EDrawDebugTrace::None,
				HitResult,
				false
			);

			if (HitResult.bBlockingHit)
			{
				return false;
			}
		}
		
		BlockList->Blocks[Index] = NewBlock;
		if (GetNetMode() == NM_DedicatedServer)
		{
			if (OldBlockData.ItemID != -1)
			{
				SpawnItemStack(BlockLocation,OldBlock,ChunkIndex);
			}

			SaveBlockModification(ChunkIndex, NewBlock, Index);
			Multicast_BlockUpdated(WorldIndex, NewBlock, OldBlock);
		}
		return true;
	}
	
	return false;
}

bool ASQWorldManager::ModifyBlock(const FIntVector& WorldIndex, int16 NewBlock)
{
	int16 OldBlock;
	return ModifyBlock(WorldIndex, NewBlock, OldBlock);
}

void ASQWorldManager::SpawnItemStack(const FVector& BlockLocation, int32 BlockId, const FIntVector& ChunkIndex)
{
	FTransform ItemStackTransform = FTransform();
	ItemStackTransform.SetLocation(BlockLocation);
	ASQItemStack* ItemStack = GetWorld()->SpawnActorDeferred<ASQItemStack>(ItemStackClass, ItemStackTransform,this,nullptr,ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	USQItem* Item;
	USQInventoryLibrary::CreateItemFromBlock(BlockId,GetWorld()->GetGameInstance(),1,Item);
	ItemStack->Item = Item;
	ItemStack->FinishSpawning(ItemStackTransform);
}

void ASQWorldManager::SaveBlockModification(const FIntVector& ChunkIndex, int16 NewBlock, int16 Index)
{
	FSQSavedChunk* SavedChunk = SavedChunks.Find(ChunkIndex);
	FSQModifiedChunkEntry Entry = FSQModifiedChunkEntry(NewBlock, Index);
	if (SavedChunk)
	{
		SavedChunk->ModifiedChunk.Modifications.Add(Entry);
		SavedChunk->ModifiedChunk.MarkItemDirty(Entry);
	}
	else
	{
		FSQSavedChunk NewSavedChunk = FSQSavedChunk();
		NewSavedChunk.ChunkIndex = ChunkIndex;
		NewSavedChunk.ModifiedChunk.Modifications.Add(Entry);
		NewSavedChunk.ModifiedChunk.MarkItemDirty(Entry);
		SavedChunks.Add(ChunkIndex,NewSavedChunk);
	}
}

void ASQWorldManager::AddActorInChunk(const FIntVector& ChunkIndex, AActor* Actor)
{
	if (FSQLoadedChunk* LoadedChunk = ChunkManager->LoadedChunks.Find(ChunkIndex))
	{
		LoadedChunk->Actors.Add(Actor);
	}
}

void ASQWorldManager::RemoveActorInChunk(const FIntVector& ChunkIndex, AActor* Actor)
{
	if (FSQLoadedChunk* LoadedChunk = ChunkManager->LoadedChunks.Find(ChunkIndex))
	{
		LoadedChunk->Actors.Remove(Actor);
	}
}

void ASQWorldManager::Multicast_BlockUpdated_Implementation(const FIntVector& WorldIndex, int16 NewBlock, int16 OldBlock)
{
	UE_LOG(LogTemp,Warning,TEXT("Block Updated at location %s | New Block: %i | Old Block : %i | NetMode : %i"),*WorldIndex.ToString(),NewBlock, OldBlock, GetNetMode());
	
	if (GetNetMode() == NM_Client)
	{
		ModifyBlock(WorldIndex, NewBlock);
	}
	
	const FIntVector ChunkIndex = USQVoxel::GetChunkIndex(WorldIndex,ChunkSize);
	ChunkManager->RenderChunks({ChunkIndex});
}
